# Escribe programas libres

Un sitio web para presentar la filosofía del <i lang="en">software</i>
libre y ayudar a ponerla en práctica.

## Contribuir

Para generar el sitio web se usa [Hugo](https://gohugo.io/).

### Traducciones

Contacta conmigo si necesitas ayuda.

## Este proyecto es una bifurcación

El proyecto original se encuentra en
<https://sr.ht/~sircmpwn/writefreesoftware.org/>. Creé esta bifurcación
porque quien mantiene el proyecto no aceptaba ninguna mejora.
