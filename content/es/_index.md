+++
title = "Escribe programas libres"
+++
{{< block "grid-2" >}}
{{< column >}}

# Creamos programas para todos

El <i lang="en">software</i> libre es un movimiento colaborativo global
para crear los mejores programas del mundo de una forma que respete los
derechos y libertades esenciales de sus usuarios y autores &mdash;la
libertad de **usar**, **estudiar**, **mejorar** y **compartir** tus
programas&mdash;.

{{< button "aprende/" "¿Qué es el «software» libre?" >}}{{< button "aprende/participa/" "Empezar" >}}
{{< /column >}}

{{< column >}}
![Foto de jáqueres en el FOSDEM](images/banner.jpg)
<small>Foto por Flo Köhler, CC BY 3.0</small>
{{< /column >}}

{{< /block >}}
