+++
author = "Drew DeVault"
title = "La guía completa para publicar videojuegos libres"
date = "2021-03-23"
description = "Una guía completa para publicar tu videojuego como software libre"
tags = ["juegos", "distribuidores"]
image = "supertuxkart.jpg"
+++

*Este artículo ha sido traducido y adaptado del [blog de Drew DeVault](https://drewdevault.com/2021/03/23/Open-sourcing-video-games.html).*

Los videojuegos son una categoría interesante de programa. A diferencia de
la mayoría de programas, son un esfuerzo creativo, más que una utilidad
práctica. Mientras que la mayoría de los programas exigen nuevas
funcionalidades para satisfacer las necesidades prácticas de sus usuarios, los
videojuegos exigen nuevas funcionalidades para servir a la visión creativa de
sus creadores. Del mismo modo, cuestiones como la refactorización y el
pago de la deuda tecnológica suelen perder prioridad en favor de la
publicación de algo lo antes posible. Muchos de los beneficios
colaborativos del <i lang="en">software</i> libre son menos aplicables a
los videojuegos. Quizá por estas razones hay muy pocos juegos
comerciales libres.

Sin embargo, existen algunos ejemplos de este tipo de juegos, y han
tenido una gran influencia en el mundo de los videojuegos. Id es famoso
por ello, ya que liberó el código fuente de varias versiones de <cite>DOOM</cite>. El
motor Quake también se publicó bajo la licencia GPL y llegó a ser muy
influyente, sirviendo de base para docenas de juegos, entre ellos
clásico como la serie <cite>Half Life</cite>. Gran parte del canon de los videojuegos
fue posible gracias a las generosas contribuciones de los publicadores de
software libre.

Publicar juegos libres es también una cuestión de preservación
histórica. Los juegos privativos tienden a atrofiarse. Mucho después de
su apogeo, con escasas plataformas adecuadas y copias físicas difíciles
de conseguir, muchos juegos mueren lenta y silenciosamente, olvidados en
los anales del tiempo. Algunos juegos han superado esta situación
liberando su código fuente, lo que facilita a los aficionados portar el
juego a nuevas plataformas y mantenerlo vivo.

¿Cuál será el legado de tu juego? ¿Será olvidado por completo, incapaz
de funcionar en las plataformas actuales? ¿Será de código disponible,
ocasionalmente útil para el jugador devoto, pero con poco
alcance más allá? Tal vez siga el camino de <cite>DOOM</cite>, viviendo para siempre
en adaptaciones a cientos de dispositivos y sistemas operativos. Tal vez
siga el camino de <cite>Quake</cite> y su alma forme parte para siempre de los
clásicos del futuro. Si mantienes el código fuente cerrado, la única
conclusión es la primera: disfrutado una vez, ahora olvidado.

Teniendo esto en cuenta, ¿cómo puedes asegurar el legado de tu juego?

## Código disponible: lo mínimo

Lo mínimo es volver tu juego de «código disponible». ¡Ten en cuenta que
esto no es lo mismo que volverlo un programa libre o de código abierto!
Algunos de tus homólogos famosos en esta categoría incluyen
<cite>Alien 3</cite>,
<cite>Civilization IV</cite> y <cite>V</cite>, <cite>Crysis</cite>, <cite>Deus Ex</cite>, <cite>Prince of Persia</cite>,
<cite>Unreal Tournament</cite> y <cite>VVVVVV</cite>.

Este método hace que tu código fuente esté disponible para verse y quizá
para compilarse y ejecutarse, pero prohíbe las obras derivadas. Sin
duda, esto es mejor que dejarlo como código cerrado: proporciona
recursos útiles para quienes crean <i lang="en">mods</i>,
<i lang="en">speedrunners</i> y otros fanes; y los jugadores devotos
pueden utilizarlo como base para hacer funcionar el juego en futuras
plataformas, aunque estén solos y no puedan compartir su trabajo.

Si optas por un modelo de restricción mínimo, algunos jugadores podrían
compartir su trabajo, pero les estarás dejando en una situación jurídica
poco sólida. Recomendaría esto si eres muy protector de tu propiedad
intelectual, pero ten en cuenta que estás limitando la segunda vida
potencial de tu juego si adoptas este sistema.

## <i lang="en">Copyleft</i> con recursos privativos

El siguiente paso es convertir tu juego en <i lang="en">software</i>
libre usando una licencia <strong><i lang="en">copyleft</i></strong>,
pero sin extender la licencia a los recursos &mdash;cualquiera que
quiera hacer funcionar el código fuente tendrá que comprarte el juego y
extraer los recursos o proporcionar sus propios recursos creados por la
comunidad&mdash;. Este es un enfoque popular entre los juegos libres y
te da la mayor parte de los beneficios y pocos inconvenientes. Te unirás
a los nuestros ejemplos de <cite>DOOM</cite> y <cite>Quake</cite>, así
como <cite>Amnesia: The Dark Descent</cite>, <cite>System Shock</cite>, <cite>Duke Nukem 3D</cite> y <cite>Wolfenstein 3D</cite>.

Este tipo de juegos disfrutan de una larga vida, ya que su
<i lang="en">software</i> es más fácil de portar a nuevas plataformas y
de compartir con otros usuarios. <cite>DOOM</cite> funciona en teléfonos,
cámaras digitales, cajeros automáticos ¡e incluso en tostadores! Su
legado está asegurado sin ningún compromiso permanente por parte de los
desarrolladores originales. Esto también permite obras derivadas
&mdash;nuevos juegos basados en tu código&mdash;, aunque podría echar
para atrás a algunos desarrolladores. El uso de una licencia
<i lang="en">copyleft</i> como la [GPL](https://www.gnu.org/licenses/gpl-3.0.html)
exige que las obras derivadas *también* se transformen en
<i lang="en">software</i> libre. La comunidad normalmente no tiene
problemas con esto, pero podría afectar a la disposición de futuros
desarrolladores a incorporar tu trabajo en sus propios juegos
comerciales. Personalmente, creo que la proliferación de
<i lang="en">software</i> libre que implica el uso de una licencia
<i lang="en">copyleft</i> es algo positivo &mdash;pero quizá quieras
usar otro enfoque&mdash;.

## Licencia permisiva, recursos propietarios

Si quieres permitir que tu código fuente llegue a tantos juegos futuros
como sea posible, una licencia de software libre permisiva como [MIT][0]
es el camino a seguir.
[Flotilla](https://github.com/blendogames/flotilla) es un ejemplo de
juego que ha adoptado este enfoque. Permite a los desarrolladores
incorporar tu código fuente en sus propios juegos con pocas
restricciones, ya sea creando un derivado directo, o tomando pequeñas
muestras de tu código e incorporándolo a su propio proyecto. Esto no
conlleva ninguna obligación de publicar sus propios cambios o trabajos
de forma similar: pueden simplemente tomarlo, con muy pocas condiciones.
Este planteamiento hace que sea muy fácil incorporarlo a nuevos juegos
comerciales.

[0]: https://opensource.org/licenses/MIT

Esta es la forma más desinteresada de liberar tu código. Yo lo
recomendaría si no te importa lo que ocurra con tu código más adelante,
y solo quieres que sea libre y pasar página. Aunque esto
definitivamente permitirá que el mayor número de futuros proyectos hagan
uso de tu trabajo, el enfoque del <i lang="en">copyleft</i> es mejor
para asegurar que el mayor número posible de futuros juegos sean
*también* libres.

## Recursos abiertos

Si te sientes especialmente generoso, también puedes liberar los
recursos. Unas buenas licencias para hacer esto son las [Creative
Commons](https://creativecommons.org/). Todas ellas permiten la libre
redistribución de tus recursos, de modo que los futuros jugadores no
tendrán que comprar tu juego para obtenerlos. Esto podría ser importante
si la plataforma de distribución que usaste ha desaparecido, o su tú ya
no vives y nadie te los puede comprar &mdash;considera esto bien antes
de decidir que prefieres quedarte con tu porcentaje de las menguantes
ventas de recursos a medida que tu juego envejece.

Usar Creative Commons también te permite ajustar la medida en que tus
recursos pueden ser reutilizados. Puedes elegir diferentes licencias CC
para controlar la comercialización de tus recursos y su uso en obras
derivadas. Para permitir la distribución gratuita y nada más, la
licencia CC NC-ND (no comercial, sin derivados) será suficiente. La
licencia CC BY-SA es el <i lang="en">copyleft</i> de Creative Commons:
permitirá la libre redistribución, comercialización y obras derivadas,
*si* las obras derivadas también son compartidas con los mismos
derechos. El enfoque permisivo es CC0, que es el equivalente a publicar
tus recursos en el dominio público.

Permitir las obras derivadas y la recomercialización de tus recursos
puede ahorrarles mucho tiempo a los nuevos desarrolladores,
especialmente a los independientes con poco presupuesto. También es
genial para crear *juegos* derivados, algo como crear *mods*, de forma
que jugadores creativos pueden combinar tus recursos para crear un nuevo
juego o extensión.

## ¿Y si no soy plenamente dueño de mi juego?

No puedes ceder los derechos de algo que no te pertenece. Si dependes
de bibliotecas propietarias, de un editor de niveles de terceros o no
posees los derechos de la música o los <i lang="en">sprites</i>, no
puedes hacer que sean libres o de código abierto.

En este caso, recomiendo publicar todo lo que puedas como
<i lang="en">software</i> libre. Esto puede significar que publiques un
juego que al final no funcione &mdash;simplemente podría no funcionar, o
ni siquiera compilar, sin estos recursos&mdash;. Es una pena, pero si
publicas todo lo que puedas, dejarás a tu comunidad en una buena
posición para llenar las lagunas por sí misma, quizá refactorizando tu
código para solucionarlas o sustituyendo las partes privativas por
alternativas libres. Esto también permite que las partes de tu juego que
son abiertas puedan reutilizarse en futuros juegos.

## ¡Pero tramposos podrían usarlo!

Esto es cierto. Y vale la pena mencionar que si tu juego tiene un
componente <i lang="en">online</i> obligatorio basado en tus propios
servidores, liberarlo no tiene mucho sentido, especialmente si
finalmente decides desconectar esos servidores.

Aquí hay que hacer concesiones. En realidad, es muy difícil evitar que
hagan trampa en tu juego. Si has creado un juego multijugador popular,
tú y yo sabemos que seguirá habiendo tramposos que lo usen, aunque
intentes evitarlo. Mantenerlo privativo no va a evitar que haya
tramposos. Las soluciones sociales son mejores &mdash;como un sistema
para denunciar a los tramposos o permitir que amigos jueguen en
servidores privados&mdash;

Liberar tu juego podría ayudar a jáqueres poco habilidosos a descubrir
cómo hacer trampas más fácilmente en tu juego. No puedo decidir por ti
si esta concesión merece la pena para tu juego, pero puedo decirte que
los beneficios de hacer que sea abierto son enormes, y la eficacia de
mantenerlo cerrado para evitar trampas es cuestionable.

## ¡Pero mi código da vergüenza!

También el de todos los demás. 🙂 Todos sabemos que los juegos tienen
plazos muy ajustados y que un código limpio no va a ser la principal
prioridad. Te aseguro que tu comunidad estará demasiado ocupada
divirtiéndose como para juzgarte por la calidad de tu código. La idea de
que primero hay que «limpiarlo» significa la muerte de muchos proyectos
que, de lo contrario, hubieran sido liberados. Si piensas así,
probablemente nunca estarás satisfecho y, por tanto, nunca lo abrirás.
Te lo aseguro: tu juego está listo para ser libre, ¡no importa en qué
estado se encuentre!

Extra: Ethan Lee me informó de un código realmente horrible que
permaneció en <cite>VVVVVV</cite>, que puedes consultar libremente en la
[etiqueta 2.2][vvvvvv 2.2]. No es genial, pero probablemente no lo sabías
&mdash;solo recuerdas <cite>VVVVVV</cite> como un juego aclamado por la
crítica&mdash;. Los desarrolladores de juegos trabajan con limitaciones
muy estrictas y nadie les juzga por ello (¡nosotros solo queremos
divertirnos!).

[vvvvvv 2.2]: https://github.com/TerryCavanagh/vvvvvv/tree/2.2

## ¿Y qué tengo que hacer?

Veamos los pasos concretos. Debes responder a las siguientes preguntas
antes:

- ¿Soy realmente propietario de todo el juego? ¿Qué partes puede publicar
  como <i lang="en">software</i> libre?
- ¿Voy a hacer que el código sea <i lang="en">copyleft</i>, de código disponible o licenciado permisivamente?
- ¿Y los recursos? ¿Privativos? ¿Creative Commons? Si elijo esto último, ¿qué versión?

Si no estás seguro de qué es lo mejor, te recomiendo que utilices la
licencia GPL para el código y CC BY-SA para los recursos. Esto permite
la creación de obras derivadas, siempre que también se abran con
una licencia similar. De este modo, la comunidad podrá desarrollar tu
obra, portarla a nuevas plataformas, crear una próspera comunidad de
creadores de <i lang="en">mods</i> y compartir libremente tus recursos,
garantizándole a tu juego un legado duradero. Si te gustaría decidir los
detalles por ti mismo, revisa los comentarios anteriores una vez más y
elige las licencias que te gustaría utilizar para cada cosa antes de
continuar.

Si necesitas ayuda con alguno de estos pasos o tienes alguna pregunta,
[envíame un correo](mailto:jorgesumle@freakspot.net), y te ayudaré en la
medida de mis posibilidades.

**Publicar el código fuente**

Combina en un archivo con tu código fuente y añade el archivo de licencia.
Si elegiste el método de código disponible, simplemente escribe
«Copyright &copy; &lt;*tú*&gt; &lt;*año actual*&gt;. Todos los derechos reservados."
en un archivo de texto llamado LICENCIA (o LICENSE).

Si quieres acabar con esto rápidamente, simplemente mete el código y la
licencia en un archivo zip o tar y colócalo en tu sitio web. Un enfoque
mejor, si tienes paciencia, sería publicarlo como un repositorio Git. Si
ya utilizas control de versiones, es posible que quieres considerar
detenidamente si quieres publicar tu historial completo de control de
versiones &mdash;la respuesta podría ser «sí», pero si no estás seguro,
probablemente la respuesta es «no». Simplemente haz una copia del
código, borra el directorio `.git` y impórtalo desde un nuevo
repositorio si lo necesitas&mdash;.

Comprueba que no estás registrando ningún artefacto &mdash;recursos,
ejecutables, bibliotecas, etc.&mdash; y luego súbelo al servicio de
alojamiento de tu elección. GitHub es una opción popular, pero yo
egoístamente recomendaría [Sourcehut](https://sourcehut.org) también. Si
tienes tiempo, escribe también un pequeño archivo LÉEME (README) que
sirva de introducción al proyecto.

**Publicar los recursos**

Si eliges dejar que los recursos sigan siendo propietarios, no hay más
pasos. Los jugadores pueden averiguar cómo extraer los recursos del
juego comprado.

Si decides hacer que sean abiertos, prepara un archivo de tus recursos.
Incluye una copia de la licencia que hayas elegido &mdash;por ejemplo,
la licencia Creative Commons que hayas utilizado&mdash; y colócala en un
archivo zip, tar o similar. Publícalo en tu sitio web y, si te sientes
generoso, prepara algunas instrucciones sobre cómo incorporar el paquete
de recursos al juego una vez que un jugador compile tu código.

**¡Cuéntaselo al mundo!**

¡Haz que todo el mundo sepa que has convertido tu juego en
<i lang="en">software</i> libre! Escribe una pequeña entrada en tu blog,
enlaza al código fuente y a los recursos y disfruta de un poco más de
protagonismo mientras la prensa y la comunidad te agradecen tu
contribución.

Una última petición a este respecto: si eliges el enfoque de código
disponible, menciónalo así en tus declaraciones públicas. Código
disponible *no* es lo mismo que «<i lang="en">software</i> libre» o
«código abierto», y la distinción es importante.

Y ahora me toca a mí darte las gracias: ¡estoy muy contento de que hayas
publicado tu juego como <i lang="en">software</i> libre! La comunidad es
mucho más rica gracias a tu contribución, y espero que tu juego viva
durante muchos años, tanto por sí mismo a través de adaptaciones y
<i lang="en">mods</i>, como en espíritu a través de sus contribuciones a
futuros juegos. Has hecho algo maravilloso. ¡Gracias!
