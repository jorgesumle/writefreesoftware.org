+++
author = "Drew DeVault"
title = "Escribe programas libres: un completo recurso educativo para el movimiento del «software» libre"
date = "2023-05-17"
description = "Esto es un nuevo sitio web para difundir la filosofía y la práctica del software libre"
tags = ["meta"]
image = "banner-blog.webp"
+++

¡Me complace anunciar hoy la publicación de [Escribe Programas Libres](https://freakspot.net/escribe-programas-libres/)!
Este sitio web es un nuevo recurso para la comunidad del <i lang="en">software</i> libre
que pretende proporcionar un punto de entrada y referencia completo y
accesible para el movimiento del <i lang="en">software</i> libre. Nuestro objetivo es
educar a otros en la filosofía y la práctica del <i lang="en">software</i> libre, para
ayudar a la gente a entender por qué el <i lang="en">software</i> libre es importante y
cómo ponerlo en práctica.

Ven aquí para aprender cosas como:

- [¿Qué es el <i lang="en">software</i> libre?](https://freakspot.net/escribe-programas-libres/aprende/)
- [¿Cómo funcionan las licencias de <i lang="en">software</i> libre?](https://freakspot.net/escribe-programas-libres/aprende/licencias/)
- [¿Cómo funciona el <i lang="en">copyleft</i>?](https://freakspot.net/escribe-programas-libres/aprende/copyleft/)
- [Cómo elegir una licencia de <i lang="en">software</i> libre](https://freakspot.net/escribe-programas-libres/aprende/participa/elegir-una-licencia/)
- [Cómo reutilizar <i lang="en">software</i> libre](https://freakspot.net/escribe-programas-libres/aprende/participa/obras-derivadas/)

Y [mucho más](https://freakspot.net/escribe-programas-libres/aprende/)!

El enfoque de contenido principal de Escribe Programas Libres
proporciona un montón de recursos informativos útiles sobre como
funciona el <i lang="en">software</i> libre y cómo se pueden aplicar sus
principios, pero eso no es todo sobre el <i lang="en">software</i>
libre. El [blog][1], aunque acaba de empezar, proporcionará muchos más
recursos de forma libre para ayudar a los mantenedores y contribuidores
a entender mejor cómo trabajar eficazmente con <i lang="en">software</i>
libre.

Futuros temas para el blog incluyen:

[1]: ..

- Crear y gestionar tus comunidades de <i lang="en">software</i> libre
- Análisis de licencias específicas de <i lang="en">software</i> libre
- Participación comercial en el <i lang="en">software</i> libre
- La importancia del <i lang="en">copyleft</i> en la práctica

¿Quieres ayudar con esto o alguna otra idea?
[¡Considera escribir para nosotros!](https://sr.ht/~sircmpwn/writefreesoftware.org/)
