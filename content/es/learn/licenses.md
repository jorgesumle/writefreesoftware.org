---
title: Licencias de «software» libre
weight: -9
slug: licencias
---

Las [cuatro libertades](https://freakspot.net/escribe-programas-libres/aprende/cuatro-libertades/) son generalmente
garantizadas mediante el uso de una **licencia de <i lang="en">software</i>
libre**. Hay muchos tipos diferentes de licencias con muchas
posibilidades para adaptarse a la situación única de cada proyecto de
<i lang="en">software</i>.

## Cómo funciona una licencia de <i lang="en">software</i> libre

Una licencia de <i lang="en">software</i> libre concede los derechos
necesarios, tal vez sujetos a algunas condiciones (p. ej., atribución),
para garantizar las cuatro libertades a los receptores del programa.
Cualquier licencia de <i lang="en">software</i> puede ser una licencia
de <i lang="en">software</i> libre si protege las cuatro libertades,
pero en la práctica la mayoría de proyectos eligen una de las numerosas
licencias populares creadas para uso general. Tratamos la información
sobre estas licencias de <i lang="en">software</i> de propósito general
y cómo elegirlas para tus propios proyectos en
[elegir una licencia](https://freakspot.net/escribe-programas-libres/aprende/participa/elegir-una-licencia/).

A menudo encontrarás una licencia de <i lang="en">software</i> libre en
el archivo `LICENSE` o `COPYING` presente en el código fuente del
programa. Otros proyectos, en particular los que combinan programas de
muchas fuentes, tienen maneras más complejas de explicar la situación de
sus licencias. Un enfoque común para gestionar esto es la
[especificación REUSE][0].

[0]: https://reuse.software/

Si quieres saber más sobre cómo funcionan en detalle las licencias de
<i lang="en">software</i> libre, sigue leyendo. En caso contrario:

{{< button "https://freakspot.net/escribe-programas-libres/aprende/participa/" "Siguiente: Participar" "next-button" >}}

## Aspectos comunes de las licencias de <i lang="en">software</i> libre

Para comprender tus obligaciones conforme a una licencia concreta,
tendrás que leerla (y quizás consultar a un abogado, especialmente si
representas a una empresa). Sin embargo, la mayoría de las licencias de
<i lang="en">software</i> libre tienen algunos rasgos en común con otras, y puede
comprenderlas de forma sencilla si aprendes algunos rasgos esenciales.
Estas son algunas características comunes de las licencias de
<i lang="en">software</i> libre:

### Atribución

Las cláusulas de atribución te obligan a **atribuir** a los autores
cuando distribuyas o reutilices programas en base a una licencia con
dicha cláusula. Por lo general esto implica reproducir la licencia
completa, o a veces un simple aviso de derecho de autor, cuando distribuyas
el programa, sus modificaciones o el nuevo programa que incorpore parte
o la totalidad del programa original.

He aquí un ejemplo de la [licencia MIT]:

> Permission is hereby granted, free of charge, to any person obtaining a copy of
> this software and associated documentation files (the “Software”), to deal in
> the Software without restriction, including without limitation the rights to
> use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
> the Software, and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
>
> <strong style="color: var(--theme)">The above copyright notice and this permission
> notice shall be included in all copies or substantial portions of the
> Software.</strong>

[licencia MIT]: https://mit-license.org

### Descargo de responsabilidad

El <i lang="en">software</i> libre suele regalarse. A cambio de este
regalo, a menudo te piden que aceptes el programa tal y como es, sin
ninguna expectativa de soporte o garantía por parte de quien publica el
programa. Este **descargo de responsabilidad** se usa para eximir de
responsabilidad a los programas libres, de modo que el receptor sea
responsable de lo que haga con él.

He aquí un ejemplo de la [licencia MIT]:

> THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

### <i lang="en">Copyleft</i>

Algunas licencias no solo *permiten* que compartas sus mejoras, sino que
*exigen* que, si compartes el programa o los programas derivados de o
que incorporan partes del original, solo puedes hacerlo usando la misma
licencia para tus mejoras. Esto es una licencia de
**<i lang="en">copyleft</i>**: una herramienta para evitar que programas
libres sean incorporados a obras no libres.

He aquí un ejemplo de la [Mozilla Public License 2.0]:

> All distribution of Covered Software in Source Code Form, including any
> Modifications that You create or to which You contribute, must be under the
> terms of this License. You must inform recipients that the Source Code Form of
> the Covered Software is governed by the terms of this License, and how they
> can obtain a copy of this License. You may not attempt to alter or restrict
> the recipients’ rights in the Source Code Form.

[Mozilla Public License 2.0]: https://www.mozilla.org/en-US/MPL/2.0/

{{< tip >}}
El <i lang="en">copyleft</i> se trata en detalle en [¿Qué es el «copyleft»?](https://freakspot.net/escribe-programas-libres/aprende/copyleft/)
{{< /tip >}}

### Compatibilidad de licencias y sublicenciamiento

La capacidad de combinar varias obras es un rasgo esencial del
ecosistema del <i lang="en">software</i> libre, pero el uso de muchas
licencias de derecho de autor puede volver este trabajo más difícil.
Aquí es donde entran en acción el **sublicenciamiento** y la
**compatibilidad de licencias**: muchas licencias de programas libres
incluyen disposiciones que permiten que sean ampliadas mediante términos
de licencias adicionales. Esto te permite combinar programas con dos o
más licencias compatibles para producir nuevos programas sujetos a los
términos de licencia de ambas.

No todas las licencias tienen términos compatibles entre sí;
concretamente, las licencias <i lang="en">copyleft</i> tienden a ser
menos compatibles con las otras. Los programas con licencias
incompatibles no pueden ser combinados en una sola obra.

{{< tip >}}
Puedes aprender más sobre compatibilidad de licencias en [reutilizando
programas libres](https://freakspot.net/escribe-programas-libres/aprende/participa/obras-derivadas/).
{{< /tip >}}

### Uso de marcas registradas y patentes

Las licencias de programas suelen ocuparse de derechos de autor, pero
quienes publican programas comerciales suelen tener otros tipos de
propiedad intelectual, como marcas y patentes. Algunas licencias de
programas libre incorporan cláusulas que abordan la relación entre los
derechos de autor del programa y otra propiedad intelectual (por ejemplo,
acordando que el uso del programa no infrinja las patentes del dueño
de los derechos de autor o prohibiendo el uso de marcas registradas del
dueño de los derechos de autor de las marcas registradas).

He aquí un ejemplo de la [licencia Apache 2.0]:

> 3. **Grant of Patent License.** Subject to the terms and conditions of this
>    License, each Contributor hereby grants to You a perpetual, worldwide,
>    non-exclusive, no-charge, royalty-free, irrevocable (except as stated in
>    this section) patent license to make, have made, use, offer to sell, sell,
>    import, and otherwise transfer the Work, where such license applies only to
>    those patent claims licensable by such Contributor that are necessarily
>    infringed by their Contribution(s) alone or by combination of their
>    Contribution(s) with the Work to which such Contribution(s) was submitted.
>    If You institute patent litigation against any entity (including a
>    cross-claim or counterclaim in a lawsuit) alleging that the Work or a
>    Contribution incorporated within the Work constitutes direct or
>    contributory patent infringement, then any patent licenses granted to You
>    under this License for that Work shall terminate as of the date such
>    litigation is filed.

[licencia Apache 2.0]: https://www.apache.org/licenses/LICENSE-2.0.html
