---
title: Gestionar la propiedad de derechos de autor
weight: 90
url: aprende/participa/propiedad-de-derechos-de-autor
---

Conviene aclarar la naturaleza de la propiedad de los derechos de autor
en el <i lang="en">software</i> libre. A falta de un acuerdo de licencia
del colaborador o un documento similar (una práctica que [desaconsejamos
encarecidamente][0]), ¿cómo gestionan los desarrolladores y publicantes
de programas libres los derechos legales asociados a los derechos de
autor de los programas?

[0]: https://freakspot.net/escribe-programas-libres/aprende/participa/contribuir/#acerca-de-los-acuerdos-de-licencia-del-colaborador

## ¿Quién posee un proyecto de <i lang="en">software</i> libre?

Cuando contribuyes a un proyecto de <i lang="en">software</i> libre, si
no cedes tus derechos de autor a otra persona, conservas la propiedad
sobre los derechos de propiedad intelectual asociados a tu contribución.
Entonces tus cambios son *licenciados* para todo el mundo, incluyendo
los otros titulares de derechos de autor, bajo los mismos términos que
la licencia con la que se distribuyó el proyecto en primer lugar.

Como tal, en la mayoría de los casos, los derechos de autor de cualquier
proyecto de <i lang="en">software</i> libre los poseen en conjunto todas
las personas que le han aportado propiedad intelectual, quienes lo
licencian para los usuarios, y entre sí, con una [licencia de «software»
libre][1].

[1]: https://freakspot.net/escribe-programas-libres/aprende/licencias/

## Determinar la procedencia

En algunos proyectos, especialmente los comerciales, podría ser
conveniente establecer lo siguiente para cada contribución:

1. El contribuidor posee los derechos de autor de su contribución o está
   autorizado por el propietario de los derechos para usarla.
2. El contribuidor acepta licenciar sus derechos de autor bajo los
   términos de la licencia del proyecto.

Si deseas que tu proyecto establezca formalmente la procedencia,
recomendamos usar el [Developer Certificate of Origin][2]. La mayoría de
proyectos facilitan esto pidiendo a los autores que «firmen» sus contribuciones.
El sistema de control de versiones [Git][3] proporciona una manera de
indicar que una contribución determinada ha sido firmada con la opción
[`git commit -s`][4].

[2]: https://developercertificate.org/
[3]: https://git-scm.com/
[4]: https://git-scm.com/docs/git-commit#Documentation/git-commit.txt--s

## Cambiar la licencia de un proyecto

Puede que en algún momento desees cambiar la licencia de tu proyecto.

[licencia permisiva]: https://freakspot.net/escribe-programas-libres/aprende/participa/elegir-una-licencia/#licencias-permisivas
[licencia <i lang="en">copyleft</i>]: https://freakspot.net/escribe-programas-libres/aprende/participa/elegir-una-licencia/#licencias-i-langencopylefti

Si el proyecto está licenciado con una [licencia permisiva], por lo general
es posible sublicenciar el proyecto original con una nueva licencia y
aplicar la nueva licencia a futuros cambios. Seguirás teniendo que
cumplir los términos de la licencia original, como la atribución, pero
los cambios futuros pueden tener una licencia con términos diferentes.
En este sentido, cambiar la licencia es algo parecido a empezar un nuevo
proyecto e incorporar en él el código original.

Sin embargo, si el proyecto está licenciado con una [licencia
<i lang="en">copyleft</i>], esto es más difícil ---muchas veces
imposible---. Es una característica intencionada de las licencias <i lang="en">copyleft</i>:
es necesario para evitar que el proyecto sea incorporado en programas no
libres. Por lo general, no puedes sublicenciar un proyecto <i lang="en">copyleft</i>
de la misma manera que puedes sublicenciar un proyecto permisivo.

Es posible cambiar la licencia de un proyecto <i lang="en">copyleft</i>,
pero tienes que
<nobr>**(a)** conseguir el</nobr> permiso de todos los propietarios de
derechos de autor o
<nobr>**(b)** reescribir sus contribuciones</nobr>. Este enfoque es
también apropiado si quieres cambiar una licencia permisiva sin tener
que estar sujeto a sus términos originales (como la atribución), pero
esto no suele merecer la pena teniendo en cuenta los términos
relativamente poco rigurosos de las licencias permisivas.

Por este motivo, en los proyectos <i lang="en">copyleft</i> se
recomienda que los derechos de autor se repartan entre todos los
colaboradores, en lugar de asignárselos a una única única entidad;
fortalece la seguridad a largo plazo del estatus de
<i lang="en">software</i> libre haciendo que sea más difícil cambiarlo a
una licencia no libre.
