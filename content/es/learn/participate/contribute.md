---
title: Contribuir a programas libres
weight: 1
url: aprende/participa/contribuir
---

Muchos proyectos de <i lang="en">software</i> libre buscan ayuda. Sea
cual sea tu área del conocimiento ---dominio de algún lenguaje de
programación, escribir documentación, gestión de comunidades,
habilidades de mercadotecnia, habilidades de diseño y arte--- hay un
proyecto por ahí al que le vendría bien tu ayuda. Fíjate en las
herramientas que utilizas a diario: lo más probable es que muchas de
ellas ya sean programas libres.

¡Sus comunidades están deseando conocerte! Busca sus salas de chat,
foros de Internet y otros lugares de encuentro y preséntate.

## Cosas que debes saber

Cuando envías mejoras a un programa libre, por lo general se espera que
las proporciones bajo los términos de la licencia de
<i lang="en">software</i> con la que ya se distribuye el proyecto.
Revisa la licencia y asegúrate de que puedes aceptar sus términos cuando
licencie tus propias contribuciones.

Si no es tuyo el trabajo que estás contribuyendo (por ejemplo, si estás
contribuyendo con el tiempo o equipamiento de tu empleador), deberás
obtener la autorización del titular de los derechos de autor. Esto
también se aplica si incorporas código de otro proyecto de
<i lang="en">software</i> libre ---consulta [reutilizando programas
libres][0] para más detalles---.

[0]: https://freakspot.net/escribe-programas-libres/aprende/participa/obras-derivadas/

## Acerca de los «acuerdos de licencia del colaborador»

Algunos proyectos te pedirán que firmes un «acuerdo de licencia del
colaborador», o un documento similar, cuando aportes tu contribución. Es
importante que lo leas con cuidado. Este documento puede servir
simplemente para verificar que posees los derechos de autor sobre tu
cambio y tienes derecho a aportarlo. Sin embargo, muchos de estos
acuerdos también pueden pedirte que renuncies a algunos de tus derechos;
por ejemplo, para permitir a quien publica el programa incorporar tus
cambios en versiones no libres del programa.

No estás obligado a renunciar a tus derechos. Estos acuerdos podrían ser
necesarios para que el distribuidor original acepte incorporar tus
cambios en su versión del programa y los distribuya en tu nombre. No
obstante, siempre tendrás derecho a distribuir una versión mejorada del
programa por tu cuenta, independientemente del distribuidor original.

Se desaconseja encarecidamente a los distribuidores de programas libres
que utilicen acuerdos de licencia del colaborador para gestionar las
contribuciones de su comunidad. Para más detalles, consulta
[gestionar la propiedad de derechos de autor][1].

[1]: https://freakspot.net/escribe-programas-libres/aprende/participa/propiedad-de-derechos-de-autor/
