---
title: Reutilizar programas libres
weight: 90
url: aprende/participa/obras-derivadas
---

Una de las grandes ventajas del <i lang="en">software</i> libre es su
potencial de reutilización. Puedes incorporar código de otros proyectos
de <i lang="en">software</i> libre en nuevos proyectos, ahorrando tiempo
y permitiéndote construir sobre los hombros de gigantes. Por supuesto,
hay que respetar el trabajo del proyecto original, y eso significa
cumplir los términos de su licencia de <i lang="en">software</i> libre.

{{< tip "warning" >}}
Lee siempre con atención la licencia cuando incorpores de otra persona
a tu propio programa.
{{< /tip >}}

## Incorporar programas permisivos en nuevas obras

El principal atractivo de las licencias de <i lang="en">software</i> es
la posibilidad de incorporarlas en cualquier cosa con relativamente
pocas obligaciones con los titulares de derechos de autor. La mayoría de
las licencias permisivas solo requieren que incluyas el texto de la
licencia, o simplemente una declaración de derechos de autor, en tu
producto. Para los proyectos de <i lang="en">software</i> libre que
incorporan código bajo licencias permisivas en sus obras, cumplir con
estas obligaciones suele ser tan sencillo como incluir una licencia
adicional con tu código fuente.

Cuando incorpores programas libres con licencias permisivas en obras que
no sean libres, debes distribuir la licencia de
<i lang="en">software</i> libre y/o la atribución de derechos de autor con
tu programa. Muchos usuarios comerciales de programas libres con licencias
permisivas incluyen un menú en algún lugar de su producto que enumera las
licencias de <i lang="en">software</i> aplicables o incluyen una
versión impresa con el producto. Deberás adoptar un enfoque similar.

## Incorporar programas <i lang="en">copyleft</i> en nuevas obras

{{< tip "warning" >}}
Los programas <i lang="en">copyleft</i> **no pueden** ser incorporados
en programas no libres.
{{< /tip >}}

La mayoría de los programas libres pueden incorporarse a programas
<i lang="en">copyleft</i>, y viceversa, si las licencias son
**compatibles**. Como regla general, la mayoría de las licencias
permisivas populares ---pero no todas--- son compatibles con la mayoría de
las licencias <i lang="en">copyleft</i> populares. *Algunas* licencias
<i lang="en">copyleft</i> son compatibles con otras licencias
<i lang="en">copyleft</i> (por ejemplo, la Mozilla Public License 2.0 es
compatible con la familia de licencias GNU), pero muchas no lo son. Dos
proyectos cualesquiera que utilicen la *misma* licencia copyleft son
compatibles entre sí y pueden compartir código libremente.

{{< tip >}}
El proyecto GNU mantiene una lista de licencias que son compatibles e
incompatibles con la familia GPL de licencias <i lang="en">copyleft</i>
[aquí][0].

[0]: https://www.gnu.org/licenses/license-list.html
{{< /tip >}}

Incorporar código permisivo a un proyecto <i lang="en">copyleft</i> es sencillo si las
licencias son compatibles: consulta la sección anterior.

{{< tip "warning" >}}
Lo contrario, incorporar un programa <i lang="en">copyleft</i> a un proyecto
de <i lang="en">software</i> con una licencia permisiva, es menos
sencillo. En este caso, la obra combinada queda sujeta a los términos de
la licencia <i lang="en">copyleft</i>. Gestionar una amalgama de
licencias permisivas y <i lang="en">copyleft</i> en una misma obra es
posible, pero tiene implicaciones importantes y significativas para tu
proyecto. Esto se suele desaconsejar encarecidamente a quienes no son
expertos: no mezcles código <i lang="en">copyleft</i> con un proyecto
permisivo a no ser que estés preparado para que tu proyecto [se pase a
una licencia <i lang="en">copyleft</i>](https://freakspot.net/escribe-programas-libres/aprende/participa/propiedad-de-derechos-de-autor/#cambiar-la-licencia-de-un-proyecto).
{{< /tip >}}

## Gestionar muchas licencias y derechos de autor en un mismo proyecto

Los proyectos de mayor envergadura y complejidad pueden incorporar
programas de muchas fuentes diferentes con varios titulares de derechos
de autor y licencias distintos. Si este es el caso de tu proyecto, te
recomendamos que consideres aplicar la especificación
[REUSE](https://reuse.software/) a tu obra.
