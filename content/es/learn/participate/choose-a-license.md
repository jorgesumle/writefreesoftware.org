---
title: Elegir una licencia
weight: 3
url: aprende/participa/elegir-una-licencia
---

Elegir una licencia es una cuestión importante a la hora de publicar tus
proyectos de <i lang="en">software</i> libre. Hay muchas entre las que
elegir, y cada una tiene diferentes ventajas e implicaciones para el
futuro de tu proyecto. Puede ser difícil [cambiar la licencia más
adelante], por lo que debes considerarlo cuidadosamente desde el
principio.

[cambiar la licencia más adelante]: https://freakspot.net/escribe-programas-libres/aprende/participa/propiedad-de-derechos-de-autor/#cambiar-la-licencia-de-un-proyecto

A continuación, te presentamos algunas de las licencias de
<i lang="en">software</i> libre más utilizadas que recomendamos y las
razonas para elegirlas.

{{< tip >}}
Una vez que elijas una licencia, inclúyela cuando compartas tu programa.
La forma más sencilla de hacerlo es poner la versión en texto plano en
un archivo llamado «COPYING» en tu repositorio de código fuente. Para
situaciones más complejas recomendamos el método [REUSE][0].

[0]: https://reuse.software/
{{< /tip >}}

## Licencias <i lang="en">copyleft</i>

Las licencias <i lang="en">copyleft</i> son útiles para garantizar que
tu programa permanece libre. El uso del <i lang="en">copyleft</i>
requiere que cualquiera que haga mejoras a tu programa las publique bajo
la mismo licencia <i lang="en">copyleft</i>, lo que asegura que puedas
incorporar sus mejoras a tu versión. Para más detalles consulta [¿Qué es
el «copyleft»?](https://freakspot.net/escribe-programas-libres/aprende/copyleft/).

{{< block "grid-2" >}}

{{< column "pros" >}}

### Ventajas

* Garantiza que tu programa siga siendo libre
* Fomenta las contribuciones comunitarias
* Promueve el <i lang="en">software</i> libre en general

{{< /column >}}

{{< column "cons" >}}

### Desventajas

* Menos atractivo para negocios
* Debe tenerse en cuenta la compatibilidad de licencias para la
  reutilización.

{{< /column >}}

{{< /block >}}

### Licencias <i lang="en">copyleft</i> recomendadas

| Licencia | Úsala para... | Enfoque <i lang="en">copyleft</i> |
| --- | --- | --- |
| [Mozilla Public License 2.0] | Bibliotecas (permite <i lang="en"><abbr title="La práctica de copiar archivos de biblioteca directamente en otro proyecto en lugar de enlazarlos por separado">vendoring</abbr></i>) | Basada en archivos |
| [GNU Lesser General Public License] | Bibliotecas (no permite <i lang="en">vendoring</i>) | Basada en objetos |
| [GNU General Public License] | Programas ejecutables | Basada en ejecución |
| [GNU Affero General Public License] | Servicios de red | Basada en red |

[Mozilla Public License 2.0]: https://www.mozilla.org/en-US/MPL/2.0/
[GNU Lesser General Public License]: https://www.gnu.org/licenses/lgpl-3.0.en.html
[GNU General Public License]: https://www.gnu.org/licenses/gpl-3.0.html
[GNU Affero General Public License]: https://www.gnu.org/licenses/agpl-3.0.html

## Licencias permisivas

Las licencias permisivas imponen relativamente pocas obligaciones al
receptor de tu programa. Estas licencias permiten que el programa se
reutilice libremente y se integre en cualquier otro proyecto de
<i lang="en">software</i>, incluido el <i lang="en">software</i> no libre.
Pueden ser útiles para proyectos cuyo objetivo sea el uso comercial o la
adopción generalizada.

{{< block "grid-2" >}}

{{< column "pros" >}}

### Ventajas

* Permite una reutilización sencilla
* Fomenta la adopción generalizada
* Atractiva para usuarios corporativos

{{< /column >}}

{{< column "cons" >}}

### Desventajas

* Puede incorporada en obras no libres
* No fomenta las contribuciones comunitarias

{{< /column >}}

{{< /block >}}

### Licencias permisivas recomendadas

Recomendamos las siguientes licencias permisivas:

* [MIT license](https://mit-license.org/)
* [BSD 3-clause license](https://opensource.org/license/bsd-3-clause/)

## Recomendadas para negocios

Para las empresas que publican programas libres, puede ser conveniente
utilizar una licencia de tipo permisivo que incluya consideraciones
sobre los derechos de marcas y patentes. Para ello recomendamos la
[licencia Apache 2.0].

[Apache 2.0 license]: https://www.apache.org/licenses/LICENSE-2.0.html

## Dominio público

Los publicantes de programas que desean incluir sus programas en el
dominio público deben tener en cuenta que una simple dedicación al
dominio público no es suficiente para el uso internacional. Recomendamos
las siguientes licencias, que proporcionan derechos legales equivalentes
a los del dominio público de forma compatible con las leyes
internacionales:

* [Creative Commons 0](https://creativecommons.org/share-your-work/public-domain/cc0/)
* [Unlicense](https://unlicense.org/)

## Licencias para otras situaciones

Tenemos una página aparte en la que se recomiendan licencias para recursos
que no sean programas, como los multimedias:

[Licenciar recursos que no son programas](https://freakspot.net/escribe-programas-libres/aprende/participa/recursos/)
