---
title: Participar
weight: 2
url: aprende/participa
---

<!-- Traducido de commit 1368c9c7 -->
Hay varias formas de participar en el movimiento del
<i lang="en">software</i> libre, como publicar tus propios proyectos de
<i lang="en">software</i> libre, contribuir a comunidades de <i lang="en">software</i>
libre exisentes, organzar eventos y activismo para la causa del
<i lang="en">software</i> libre y más.

{{< button "https://freakspot.net/escribe-programas-libres/aprende/participa/contribuir/" "Aprende a contribuir" >}}{{< button "https://freakspot.net/escribe-programas-libres/aprende/participa/publicar/" "Aprende a publicar" >}}
