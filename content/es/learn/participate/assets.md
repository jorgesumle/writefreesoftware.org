---
title: Licenciar cosas que no son programas
weight: 10
url: aprende/participa/recursos
---

Las licencias de <i lang="en">software</i> libre son las más adecuadas
para licenciar... <i lang="en">software</i>. Sin embargo, los proyectos
de <i lang="en">software</i> libre a menudo incorporan multimedias, que
no es <i lang="en">software</i> en sí, como material gráfico y
documentación. Para estos casos se recomiendan licencias diferentes.
Tenemos algunas recomendaciones para licencias que son adecuadas para
multimedias que no son <i lang="en">software</i>, respetan los
principios del <i lang="en">software</i> libre y son compatibles con
licencias de <i lang="en">software</i> libre.

## Creative Commons

La mayoría de recursos multimedia ---imágenes, audio, vídeos, escritos,
etc.--- son adecuados para su uso con licencias [Creative Commons][0],
que incluyen opciones configurables para aspectos como el
<i lang="en">software</i> y la atribución. No obstante, ten en cuenta
que las variantes -ND (no derivadas) y -NC (no comercial) son
incompatibles con el <i lang="en">software</i> libre, y el uso de estos
recursos limitará la utilidad de tu proyecto dentro del ecosistema del
<i lang="en">software</i> libre.

[0]: https://creativecommons.org/

## <i lang="en">Hardware</i>

Recomendamos que los proyectos de <i lang="en">hardware</i> (esquemas, fuentes HDL, etc.) usen la
<a href="https://cern-ohl.web.cern.ch/home" class="non-free" title="Este enlace te llevará a una página no libre">CERN Open Hardware License</a>.

## Fuentes

La
<a href="https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL" class="non-free" title="Este enlace te llevará a una página no libre">SIL Open Font License</a>
está recomendada para distribuir fuentes de forma compatible con los
programas libres.

## Documentación

La mayoría de proyectos no usan una licencia especial para su
documentación. Sin embargo, la [GNU Free Documentation License][fdl]
a veces se usa para este propósito.

[fdl]: https://www.gnu.org/licenses/fdl-1.3.html
