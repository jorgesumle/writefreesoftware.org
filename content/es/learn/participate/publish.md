---
title: Publicar programas libres
weight: 2
url: aprende/participa/publicar
---

¿Se te ha ocurrido una idea genial para un nuevo proyecto de
<i lang="en">software</i> libre y estás deseando publicarlo?
¡Genial! Aquí tienes los pasos:

1. ¡Escríbelo!
2. Elige una licencia de <i lang="en">software</i>
3. Publica tu proyecto
4. Crea una comunidad (opcional)

## Escribir tu programa

No podemos ayudarte mucho con esta parte, claro. Sin embargo, un consejo:
**publica pronto**. Muchos nuevos mantenedores dudan a la hora de
publicar su código, preocupados de que la documentación sea mala, falten
funcionalidades, abunden los errores o, incluso, sea malo el código
---todo tiene que ser arreglado para que esté listo para el mundo---.
¡No es así! Publicar pronto es un paso crucial para obtener opiniones
rápidamente y es esencial para atraer a otros colaboradores. Publica tu
código lo antes posible.

## Elegir una licencia

Sin embargo, antes de publicar, debes elegir una licencia de
<i lang="en">software</i> libre que se adapte a tus necesidades.
*Puedes* escribir una por tu cuenta, pero es **totalmente**
desaconsejable, incluso para grandes empresas que pueden contratar un
abogado para que les ayude. Hay un montón de licencias disponibles para
ajustarse a cualquier necesidad, y explicamos cómo elegir la adecuada
para ti en [elegir una licencia][0].

[0]: https://freakspot.net/escribe-programas-libres/aprende/participa/elegir-una-licencia/

Elegir una licencia es **obligatorio** cuando se publica un proyecto de
<i lang="en">software</i> libre ---a falta de una licencia tu trabajo no
es libre---. Tu elección también puede tener consecuencias
significativas a largo plazo para tu proyecto, así que elige con
cuidado. Este es un paso importante.

## Publicar tu proyecto

Cuando estés listo para publicar tu obra, la forma más sencilla es
subirla a una **forja de programas**. Hay muchas entre las que elegir, y
cada una ofrece numerosas herramientas para facilitarte el desarrollo y
la colaboración. Muchas de estas forjas de programas son en sí libres
---recomendamos usar estas en vez de las alternativas no libres---. Aquí
tienes algunas sugerencias:

- [Codeberg](https://codeberg.org)
- [SourceHut](https://sourcehut.org)

También puedes alojar tu proyecto en tu propia infraestructura. Algo tan
simple como un archivo tar en un servidor web sirve, pero también puedes
llevar una forja de programas completa en tu propio centro de datos si
eso es mejor para tu proyecto.

## Crear una comunidad

Una de las principales ventajas de publicar programas libres es que tu
comunidad puede involucrarse para mejorarlos más de lo que tú por tu
cuenta puedes. Llevar una comunidad es importante para asegurarte de que
tu programa es lo mejor que puede ser. [Publicamos artículos][blog]
sobre varios temas de interés para quienes interactúan con programas
libres, incluyendo consejos sobre creación y gestión de comunidades.
¡Échale un vistazo!

[blog]: ../../../blog/
