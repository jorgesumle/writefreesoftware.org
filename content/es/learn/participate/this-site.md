---
title: Contribuir a este sitio web
weight: 1000
url: aprende/participa/contribuir-a-este-sitio-web
---

Este sitio web es libre. El código fuente del contenido y el diseño está
disponible [en Codeberg][0], una plataforma libre de desarrollo de programas.
¡Sigue el enlace para información sobre cómo contribuir mejoras! Este
sitio web y su contenido usa la licencia
Creative Commons Attribución-CompartirIgual 4.0 Internacional
([CC BY-SA 4.0][CC-BY-SA]), una licencia <i lang="en">copyleft</i>.

[0]: https://codeberg.org/jorgesumle/writefreesoftware.org
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/deed.es

El tema está basado en el tema de Hugo
<a href="https://github.com/onweru/compose" class="non-free"
title="Advertencia: este enlace te llevará a GitHub, un sitio web no
libre">compose</a>, que es un programa libre bajo la [licencia
MIT](https://mit-license.org/), &copy; 2020 Weru.
