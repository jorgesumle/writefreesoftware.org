---
title: ¿Qué es el «copyleft»?
weight: -8
slug: copyleft
---

El **copyleft** es una herramienta para licenciar única del
<i lang="en">software</i> libre. Está diseñada para fomentar la proliferación
del programas libres y evitar que los programas libres se incorporen a
productos no libres. Funciona dándote no solo el *derecho* a compartir
tus mejoras, sino también la *obligación* a compartir tus mejoras en
algunos casos. Es muy importante entender estas obligaciones al
reutilizar programas <i lang="en">copyleft</i> en tus propios proyectos.

{{< tip >}}
**Nota terminológica**:
Confundir «<i lang="en">copyleft</i>» con «software <i lang="en">libre</i>»
es un error común: el <i lang="en">software</i> que no es
<i lang="en">copyleft</i> puede ser libre, y los programas abiertos pueden
ser <i lang="en">copyleft</i>. Sin embargo, quienes se sienten más
identificados con el movimiento del <i lang="en">software</i> libre
suelen estar más a favor de las licencias <i lang="en">copyleft</i> que
quienes se identifican con el movimiento del código abierto.
{{< /tip >}}

## El repertorio <i lang="en">copyleft</i>

Existen diferentes licencias de <i lang="en">software</i> libre en un
abanico que va de las **permisivas** a las **<i
lang="en">copyleft</i>**, basándose en el énfasis que le dan al
<i lang="en">copyleft</i> en sus términos de licencia. Las licencias
permisivas suelen permitir una reutilización generosa con relativamente
escasas y poco onerosas, como simples requisitos de atribución. Por el
contrario, las licencias <i lang="en">copyleft</i> imponen la obligación
de compartir tus cambios y obras derivadas bajo los mismos términos de
licencia.

<img src="../../images/es/licensing-spectrum.svg" alt="varios proyectos y licencias organizados en categorías" />
<small>
  Varias licencias de programas y ejemplos de proyectos que las usan,
  organizadas en la gama de <i lang="en">copyleft</i>. Gráfico original
  por David A Wheeler, <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.es">CC BY-SA 3.0</a>.
</small>

## ¿Por qué elegir una licencia <i lang="en">copyleft</i>?

Es frecuente que programas con licencia permisiva sean incorporados a
obras que no son libres. Esto se hace a menudo con el fin de obtener
mayores beneficios negando las cuatro libertades a los usuarios que
reciben la obra libre, haciendo un uso lucrativo del programa sin
retribuir nada a la comunidad del <i lang="en">software</i> libre.

Las licencias <i lang="en">copyleft</i> abordan algunos de estos
problemas:

1. El <i lang="en">copyleft</i> promueve la proliferación de programas libres
   y las cuatro libertades garantizando que el trabajo realizado sobre
   el <i lang="en">software</i> libre crezca y beneficie al ecosistema del
   <i lang="en">software</i> libre.
2. El <i lang="en">copyleft</i> garantiza que quienes mejoran o reutilizan el
   <i lang="en">software</i> libre compartan sus cambios con la comunidad,
   para que todos los usuarios puedan beneficiarse de sus mejoras.

Los programas <i lang="en">copyleft</i> pueden venderse, como el resto
de programas libres, pero requerir que las mejoras comerciales sigan
siendo libres garantiza que las cuatro libertades sean respetadas por
todos los actores. Es más, es difícil cambiar la licencia de programas
<i lang="en">copyleft</i> si el derecho de autor es poseído [en conjunto][0],
lo cual sirve como una promesa sólida para el futuro del programa como
<i lang="en">software</i> libre.

[0]: https://freakspot.net/escribe-programas-libres/aprende/participa/propiedad-de-derechos-de-autor/

## <i lang="en">Copyleft</i> débil y fuerte

Las licencias <i lang="en">copyleft</i> se diferencian en la medida en
que sus cláusulas afectan a la reutilización del programa. Por ejemplo,
la licencia de <i lang="en">copyleft</i> débil [Mozilla Public License][MPL] está
*basada en archivos*, de forma que la cláusula de <i lang="en">copyleft</i> cubre
archivos de código fuente individuales y no el proyecto como conjunto: puedes
poner uno de estos archivos en cualquier proyecto sin tener que
relicenciar el proyecto mayor, siempre que vuelvas a publicar cualquier
cambio a esos archivos específicos.

[MPL]: https://www.mozilla.org/en-US/MPL/2.0/

Un ejemplo algo más fuerte es la licencia [GNU Lesser General Public License][LGPL],
que está pensada específicamente para bibliotecas de programas. Estas
bibliotecas son compiladas en un artefacto de <i lang="en">software</i>
agregad, como un objetivo compartido o un archivo estático, y los
términos del <i lang="en">copyleft</i> se aplican a todo este artefacto.
Sin embargo, cuando se enlaza a esto con un tercer programa, la cláusula
de <i lang="en">copyleft</i> no es invocada. Más fuerte aún es la
[GNU General Public License][GPL], que trata el programa completo como
el artefacto de <i lang="en">software</i> al que se aplica la cláusula
de <i lang="en">copyleft</i>.

[LGPL]: https://www.gnu.org/licenses/lgpl-3.0.en.html
[GPL]: https://www.gnu.org/licenses/gpl-3.0.html

En el extremo están las licencias como la
[GNU Affero General Public License][AGPL], que amplía la
<abbr title="GNU General Public License">GPL</abbr> para aplicarla a los
programas usados en red, como bases de datos, y considera a los usuarios
de ese programa los «receptores» del programa, que tienen, por tanto,
derecho a recibir el código fuente.

[AGPL]: https://www.gnu.org/licenses/agpl-3.0.html

## Cómo reutilizar obras <i lang="en">copyleft</i>

La forma más simple de reutilizar obras <i lang="en">copyleft</i> es
aplicar su licencia a tu propia obra y distribuirla en base a ella.

Si no quieres hacer esto, solo podrás usar una obra
<i lang="en">copyleft</i> bajo las condiciones permitidas por su
licencia, y probablemente estés limitado a usar obras de
<i lang="en">copyleft</i> débil. Por ejemplo, si tu programa depende de
una biblioteca que usa la licencia <abbr title="GNU Lesser General Public License">LGPL</abbr>,
puedes usar cualquier licencia para tu obra, pero tienes que compartir
los cambios que haces a la biblioteca en sí. Si el programa usa la
licencia GPL o AGPL, estarás más restringido. Lee con atención los términos
de licencia y consulta a un abogado si no estás seguro de cómo proceder.

Para más información consulta nuestra página sobre [reutilizar programas libres](../participa/obras-derivadas/).

{{< tip >}}
La [Software Freedom Conservancy][sfc] es una organización que, entre
otras cosas, busca soluciones legales para que el
<i lang="en">copyleft</i> sea respetado. Para saber más sobre el cumplimiento
del <i lang="en">copyleft</i> para tus propios proyectos, consulta sus
recursos.

[sfc]: https://sfconservancy.org/
{{< /tip >}}
