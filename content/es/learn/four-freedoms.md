---
title: Las cuatro libertades
slug: cuatro-libertades
weight: -10
---

<blockquote>
  <ol start="0">
    <li>
      La libertad de <strong>usar</strong> el programa para cualquier fin.
    </li>
    <li>
      La libertad de <strong>estudiar</strong> y <strong>mejorar</strong> el
      programa.
    </li>
    <li>
      La libertad de <strong>compartir</strong> el programa.
    </li>
    <li>
      La libertad de <strong>colaborar</strong> en el desarrollo del programa.
    </li>
  </ol>
</blockquote>

Examinemos cada una de las cuatro libertades con más detalle.

## 0: Usar el programa

La libertad «cero» garantiza le garantiza a todo el mundo el derecho de **usar** el programa para cualquier propósito, incluido el uso comercial ---aunque parezca contradictorio, se puede vender---. También puedes incorporar programas libres a tu propio trabajo, pero ten cuidado: sobre este asunto hay algunas cuestiones que trataremos en [Reutilizar programas libres](https://freakspot.net/escribe-programas-libres/aprende/participa/obras-derivadas/).

{{< tip >}}
En la jerga del <i lang="en">software</i> libre esto suele llamarse
el requisito de «no discriminación».
{{< /tip >}}

## 1: Estudiar y mejorar el programa

La primera libertad garantiza el derecho a **estudiar** el
comportamiento del programa. ¡Tienes derecho a entender cómo funciona el
programa del que dependes! Para facilitar esto el código fuente debe ser
incluido con el programa. Quienes publican programas libres no pueden
ofuscar el código fuente, prohibir la ingeniería inversa o evitar de
otra forma que hagas uso de ellos.

Además, tienes un derecho a **mejorar** el código fuente. No solo se te
da el derecho a leer el código fuente de un programa libre, sino también
a cambiarlo para que se ajuste mejor a tus necesidades.

## 2: Compartir el programa

La segunda libertad garantiza el derecho a **compartir** programas
libres con otros. Si tienes la copia de un programa libre, puedes
dársela a tus amigos, publicarla en tu sitio web o incluirla en otro
programa y compartirla como parte de algo mayor. También puedes
compartir las mejoras que le hagas ---¡así es mejor para todo el
mundo!---.

{{< tip >}}
Puedes cobrarles una tasa a las personas con las que compartes el programa
para cubrir los costes del ancho de banda, por ejemplo. Sin embargo, ten
en cuenta que quien lo recibe tiene el derecho de compartirlo, sin pagar
un canon.
{{< /tip >}}

## 3: Colaborar en el desarrollo del programa

La tercera libertad garantiza un derecho adicional: el derecho a
**colaborar** con otros en la mejora del programa. Puedes estudiar,
mejorar y compartir el código fuente, y las personas con las que lo
compartes pueden estudiarlo, mejorarlo y compartirlo de nuevo contigo.
En esto se basa el **movimiento del <i lang="en">softwaree</i> libre**:
una comunidad global de entusiastas del <i lang="en">software</i> que
comparten y mejoran programas juntos.

{{< button "https://freakspot.net/escribe-programas-libres/aprende/licencias/" "Siguiente: Licencias de programas libres" "next-button" >}}
