---
title: Sobre el «software» libre
weight: 1
---

> «<i lang="en">Software</i> libre» son programas desarrollados y publicados
> según los principios que respetan los derechos y libertades esenciales
> de sus usuarios y autores &mdash;la libertad de **usar**,
> **estudiar**, **mejorar** y **compartir** los programas&mdash;
> <wbr/>El **movimiento del <i lang="en">software</i> libre** es una
> comunidad global de desarrolladores de <i lang="en">software</i> y
> usuarios que colaboran creando programas según estos principios.

Cualquier programa se considera <i lang="en">software</i> libre siempre
que defienda las *cuatro libertades esenciales*:

<ol start="0">
  <li>
    La libertad de <strong>usar</strong> el programa para cualquier
    propósito.
  </li>
  <li>
    La libertad de <strong>estudiar</strong> y <strong>mejorar</strong>
    el programa.
  </li>
  <li>
    La libertad de <strong>compartir</strong> el programa.
  </li>
  <li>
    La libertad de <strong>colaborar</strong> en el desarrollo del
    programa.
  </li>
</ol>

Los programas que defienden estas tres libertades son **libres**. Los
programas que no son **no libres**.

{{< button "cuatro-libertades/" "Siguiente: Las cuatro libertades" "next-button" >}}

## ¿Qué es el <i lang="en">software</i> de código abierto?

El movimiento del **código abierto** es parecido al movimiento del <i lang="en">software</i>
libre. Los movimientos difieren principalmente en el público al que se
dirigen: el código abierto es más comercial, y el <i lang="en">software</i>
libre se centra más en los usuarios. No obstante, ambos movimientos
están estrechamente relacionados y a menudo trabajan juntos. Cada
movimiento ofrece una visión diferente de la libertad del <i lang="en">software</i>,
pero en la práctica casi todos los programas que son considerados libres
también se consideran de código abierto y viceversa. La definición de
código abierto y las cuatro libertades son compatibles entre sí.

El conjunto de ambos movimientos suele denominarse
«<i lang="en">software</i> libre y de código abierto».

{{< tip >}}
En general, todos los programas de código abierto son libres, y viceversa.
{{< /tip >}}

Puedes aprender más sobre el código abierto en
[opensource.org](https://opensource.org/).

## ¿Qué es el <i lang="en">software</i> de «código disponible»?

<i lang="en">Software</i> de «código disponible» hace referencia a
cualquier programa cuyo código fuente esté disponible, el cual podría o
no respetar las cuatro libertades. Podría limitar el uso comercial,
restringir la redistribución, impedir que el usuario modifique el
programa, etc. Todos los programas libres y de código abierto son de
código disponible, pero no todos los programas de código disponible son
libres.

{{< tip "warning" >}}
Los programas de «código disponible» suelen ser no libres.
{{< /tip >}}
