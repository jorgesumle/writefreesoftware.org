---
title: Getting involved
weight: 2
---

There are many ways to get involved with the free software movement, including
publishing your own free software projects, contributing to existing free
software communities, organizing events and activism for the cause of free
software, and more.

{{< button "contribute/" "Learn about contributing" >}}{{< button "publish/" "Learn about publishing" >}}
