+++
title = "Skribu liberajn programojn"
+++
{{< block "grid-2" >}}
{{< column >}}

# Ni kreas programojn por ĉiuj

La movado por liberaj programoj estas tutmonda kunlabora movado por krei
la plej bonajn programojn de la mondo per maniero, kiu respektas la
esencajn rajtojn kaj liberecojn de iliaj uzantoj kaj aŭtoroj &mdash; la
liberecon **uzi**, **studi**, **plibonigi** kaj **kunhavigi** viajn
programojn.

{{< button "lerni/" "Kio estas liberaj programoj?" >}}{{< button "lerni/partopreni/" "Komenciĝi" >}}
{{< /column >}}

{{< column >}}
![Foto de kodumuloj en FOSDEM](../images/banner.jpg)
<small>Foto de Flo Köhler, CC BY 3.0</small>
{{< /column >}}

{{< /block >}}
