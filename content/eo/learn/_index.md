---
title: Pri liberaj programoj
weight: 1
---

> «Liberaj programoj» estas programoj disvolvitaj kaj eldonitaj laŭ
> principoj, kiuj respektas la esencajn rajtojn kaj liberecojn de iliaj
> uzantoj kaj aŭtoroj &mdash; la liberecon **uzi**, **studi**,
> **plibonigi** kaj **kunhavigi** viajn programojn. <wbr/>La **movado
> por la liberaj programoj** estas tutmonda komunumo de programistoj kaj
> uzantoj, kiuj kunlaboras por krei programojn laŭ tiuj principoj.

Oni konsideras iun ajn programon kiel libera, nur se ĝi defendas la
*kvar esencajn liberecojn*:

<ol start="0">
  <li>
    La libereco <strong>uzi</strong> la programo por ajna celo.
  </li>
  <li>
    La libereco <strong>studi</strong> kaj <strong>plibonigi</strong> la
    programon.
  </li>
  <li>
    La libereco <strong>kunhavigi</strong> la programon.
  </li>
  <li>
    La libereco <strong>kunlabori</strong> por disvolvi la programon.
  </li>
</ol>

Programo, kiu defendas tiujn liberecojn, estas **libera programo**.
Programo, kiu ne faras tion, estas **nelibera**.

{{< button "kvar-liberecoj/" "Sekvas: La kvar liberecoj" "next-button" >}}

## Kio signifas «malfermitkoda» programo?

**Malfermitkodo** estas movado simila al la movado por la liberaj programoj.
La movadoj malsamas ĉefe pri siaj celgrupoj: la malfermitkoda
fokusiĝas pli pri komercaj aferoj, kaj la movado por la liberaj
programoj temas pli pri la uzantoj. Tamen ambaŭ movadoj estas tre rilata
kaj ofte kunlaboras. Ĉiu movado donas malsaman vidon de la
programarlibereco, sed praktike preskaŭ ĉiuj programoj, kiuj estas
konsideritaj kiel liberaj, estas ankaŭ konsideritaj kiel malfermitkodaj
kaj inverse. La difino pri malfermitkodo kaj la kvar liberecoj estas
kongruaj unu kun la alia.

Oni ofte nomas la du movadojn kiel tuton «movado por liberaj kaj
malfermitkodaj programoj».

{{< tip >}}
Ĝenerale ĉiuj malfermitkoda programo estas libera kaj inverse.
{{< /tip >}}

Vi povas lerni pli pri malfermitkodo en
[opensource.org](https://opensource.org/).

## Kio estas «koddisponebla» programo?

«Koddisponebla» programo estas iu ajn programo, kies fontkodo
disponeblas, kiu povus aŭ ne defendi la kvar liberecojn. Ĝi povus limigi
komercan uzon; restrikti redistribuon; malhelpi, ke la uzanto modifu la
programon; kaj tiel plu. Ĉiuj liberaj kaj malfermitkodaj programoj estas
koddisponeblaj, sed ne ĉiuj koddisponeblaj programoj estas liberaj.

{{< tip "warning" >}}
«Koddisponebla» programo estas ofte nelibera.
{{< /tip >}}
