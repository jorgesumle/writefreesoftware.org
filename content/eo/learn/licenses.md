---
title: Permesiloj de liberaj programoj
weight: -9
slug: permesiloj
---

La [kvar liberecoj](../kvar-liberecoj/) estas precipe garantiataj per la
uzo de **liberprograma permesilo**. Estas multaj malsamaj specoj de
permesiloj kun multaj malsamaj ebloj adaptiĝi al la unika situacio de
ĉiu programa projekto.

## Kiel liberprograma permesilo funkcias

Liberprograma permesilo donas la necesajn rajtojn, eble kondiĉitajn de
kelkaj admonoj (ekzemple atribuo), por estigi la kvar liberecoj por
ricevantoj de la programo. Ajna programa permesilo povas esti
liberprograma permesilo, se ĝi defendas la kvar liberecojn, sed praktike
la plejmulto da projektoj elektas unu el la multaj popularaj permesiloj
kreitaj por ĝenerala uzo. Informo pri tiuj ĝeneralcelaj
programpermesiloj kaj kiel elekti inter ili por viaj propraj projektoj
estas donita en [elekti licencon](../partopreni/elekti-permesilon).

Ofte vi trovos liberprograman permesilon en la dosiero `LICENSE` aŭ
`COPYING` estanta en la programa fontkodo. Aliaj projektoj, specife
tiuj kiuj kombinas programojn de multaj devenoj, havas kompleksajn
manierojn klarigi sian licencan situacion. Kutima metodo administri tion
estas la [REUSE-normo][0].

[0]: https://reuse.software/

Se vi volas detale scii kiel liberprogramaj permesiloj funkcias, legu
plu. Alie:

{{< button "../partopreni/" "Sekvas: Partopreni" "next-button" >}}

## Komunaj ecoj de liberprogramaj permesiloj

Por kompreni viajn devigojn sub iu ajn permesilo, vi devos legi ĝin (kaj
eble konsulti advokaton, speciale se vi reprezentas firmaon). Tamen la
plejparto de liberprogramaj permesiloj havas iujn ecojn komunajn kun
aliaj, kaj vi povas baze kompreni ilin lerante pri kelkaj esencaj ecoj.
Tie ĉi estas iuj komunaj ecoj de liberprogramaj permesiloj:

### Atribuo

Atribuo-klaŭzoj postulas, ke vi **atribuu** al la aŭtoroj, kiam vi
distribuas aŭ reuzas programojn bazitajn sur permesiloj kun tia klaŭzo.
Ĉi tiu kutime necesigas plene kopii la permesilon, aŭ iufoje simpla
aŭtorrajta noto, kiam vi distribuas la programon, modifojn al ĝi aŭ
novan programon, kiu inkludas ion aŭ ĉion el la originala programo.

Jen ekzemplo el la [MIT-permesilo]:

> Permission is hereby granted, free of charge, to any person obtaining a copy of
> this software and associated documentation files (the “Software”), to deal in
> the Software without restriction, including without limitation the rights to
> use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
> the Software, and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
>
> <strong style="color: var(--theme)">The above copyright notice and this permission
> notice shall be included in all copies or substantial portions of the
> Software.</strong>

[MIT-permesilo]: https://mit-license.org

### Forlaso de garantio

Liberaj programoj ofte estas donitaj kiel donacoj. Kontraŭ tiu donaco
oni ofte petas al vi, ke vi akceptu la programo kiel ĝi estas, sen nenia
atendoj de subteno aŭ garantio de la eldonisto. Ĉi tiu **forlaso de
garantio** estas uzata por forlasi respondecon pri libera programo, tiel
ke la ricevanto respondecas pri tio, kion ri faras per ĝi.

Jen ekzemplo el la [MIT-permesilo]:

> THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

### Rajtocedo

Iuj permesiloj ne nur *permesas* al vi kunhavigi viajn plibonigojn, sed
*postulas*, ke se vi kunhavigas la programon aŭ programon, kiu derivas
de la originala aŭ enhavas partojn de la originala, vi nur povas fari
tion uzante la saman permesilon por viaj plibonigoj. Ĉi tio estas
**rajtoceda** permesilo: ilo por eviti, ke liberaj programoj estu
inkluditaj en ne-liberajn verkojn.

Jen ekzemplo el la [Mozilla Public License 2.0]:

> All distribution of Covered Software in Source Code Form, including any
> Modifications that You create or to which You contribute, must be under the
> terms of this License. You must inform recipients that the Source Code Form of
> the Covered Software is governed by the terms of this License, and how they
> can obtain a copy of this License. You may not attempt to alter or restrict
> the recipients’ rights in the Source Code Form.

[Mozilla Public License 2.0]: https://www.mozilla.org/en-US/MPL/2.0/

{{< tip >}}
La rajtocedo estas detale klarigita en [Kio estas rajtocedo?](../rajtocedo/)
{{< /tip >}}

### Kongrueco de licencoj kaj sublicencado

La kapablo kombini multajn verkojn estas esenca trajto de la
medio de liberaj programoj, sed la uzo de multaj malsamaj aŭtorrajtaj
permesiloj povas malfaciligi ĉi tiun laboron. Pro tio naskiĝis la
**sublicencado** kaj la **kongrueco de licencoj**: multaj liberprogramaj
licencoj enhavas kondiĉojn, per kiuj ili povas esti etenditaj per la
kondiĉoj de kromaj licencoj. Ĉi tio ebligas al vi kombini programojn kun
du aŭ pli kongruaj licencoj por produkti novan programon submetita al la
licenckondiĉoj de ambaŭ.

Ne ĉiuj licencoj havas kondiĉoj, kiuj kongruas unu la alian; specife
rajtocedaj licencoj kutime estas malpli kongruaj kun aliaj. Programoj
kun nekongruaj licencoj ne povas esti kombinitaj en unu verko.

{{< tip >}}
Vi povas lerni pli pri kongrueco de licencoj en [Reuzi liberajn programojn](../partopreni/derivitaj-verkoj/).
{{< /tip >}}

### Uzo de varmarkoj kaj patentoj

Programlicencoj ĝenerale traktas aŭtorajn rajtojn, sed komercaj
eldonistoj de programoj ofte tenas alian specon de intelekta propraĵo,
kiel varmarkojn kaj patentojn. Kelkaj liberprogramaj licencoj havas
klaŭzojn, kiuj traktas la rilaton inter la programa aŭtorrajto kaj alia
intelekta propraĵo, ekzemple konsentante, ke la uzo de la programo ne
malobservas la patentojn de la aŭtorrajta posedanto, aŭ malpermesante la
uzon de la varmarkoj de la aŭtorrajta havanto.

Jen ekzemplo de la [permesilo Apache 2.0]:

> 3. **Grant of Patent License.** Subject to the terms and conditions of this
>    License, each Contributor hereby grants to You a perpetual, worldwide,
>    non-exclusive, no-charge, royalty-free, irrevocable (except as stated in
>    this section) patent license to make, have made, use, offer to sell, sell,
>    import, and otherwise transfer the Work, where such license applies only to
>    those patent claims licensable by such Contributor that are necessarily
>    infringed by their Contribution(s) alone or by combination of their
>    Contribution(s) with the Work to which such Contribution(s) was submitted.
>    If You institute patent litigation against any entity (including a
>    cross-claim or counterclaim in a lawsuit) alleging that the Work or a
>    Contribution incorporated within the Work constitutes direct or
>    contributory patent infringement, then any patent licenses granted to You
>    under this License for that Work shall terminate as of the date such
>    litigation is filed.

[permesilo Apache 2.0]: https://www.apache.org/licenses/LICENSE-2.0.html
