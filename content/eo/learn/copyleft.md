---
title: Kio estas rajtocedo?
weight: -8
slug: rajtocedo
---

**Rajtocedo** estas permesilo de liberaj programoj. Ĝi estas desegnita
por instigi la disvastiĝon de liberaj programoj kaj eviti, ke liberaj
programoj estu integritaj en neliberajn verkojn. Ĉi tio funkcias donante
al vi ne nur la *rajton* kunhavigi viajn plibonigojn, sed ankaŭ la
*devigon* kunhavigi sub la samaj kondiĉoj. Estas tre grave kompreni ĉi
tiujn devigojn, kiam vi reuzas rajtocedajn programojn en via propra
verko.

{{< tip >}}
**Terminologia noto**:
Konfuzi «rajtocedon» kaj «programliberecon» estas kutima miskompreno:
ne-rajtoceda programo povas esti libera, kaj malfermitkoda programo
povas esti rajtoceda. Tamen tiuj, kiuj identigas sin pli kun la
movado por la liberaj programoj, kutime estas pli favoraj al la
rajtocedaj permesiloj ol tiuj, kiuj identigas sin pli kun la
malfermitkoda movado.
{{< /tip >}}

## La rajtoceda spektro

Ekzistas malsamaj liberprogramaj permesiloj en spektro, el **permesemaj**
al **rajtocedaj**, bazita sur kiom ili substrekas la rajtocedon en iliaj
licencaj kondiĉoj. Permesemaj licencoj emas permesi la reuzadon kaj havas
relative malmultajn kaj malpezajn devigojn, kiel simplajn postulojn
atribui. Kontraste rajtocedaj permesiloj trudas la devigon kunhavigi
viajn ŝanĝojn kaj derivitajn verkojn sub la samaj licencaj kondiĉoj.

<img src="../../../images/eo/licensing-spectrum.svg" alt="diversaj projektoj kaj licencoj organizitaj laŭ spektro" />
<small>
  Diversaj programaj licencoj kaj ekzemploj de projektoj, kiuj uzas
  ilin, organizitaj en la rajtoceda spektro. Origina bildo de David A
  Wheeler, <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.eo">CC BY-SA 3.0</a>.
</small>

## Kial elekti rajtocedan permesilon?

Kutimas, ke permese licencitaj liberaj programoj estu integritaj en
malliberajn verkojn. Oni faras ofte tion pro akiri pli grandajn
profitojn neante la kvar liberecojn al uzantoj, kiuj ricevas la
ne-liberan verkon, profite uzante la programon sen redoni ion al la
komunumo de liberaj programoj.

Rajtocedaj permesiloj traktas iujn el tiuj ĉi problemoj:

1. Rajtocedo antaŭenigas la disvastiĝon de liberaj programoj kaj la kvar
   liberecojn certigante, ke laboro farita sur liberaj programoj kreskigas
   kaj profitas la medion de liberaj programoj.
2. Rajtocedo certigas, ke tiuj, kiuj plibonigas aŭ reuzas liberajn programojn,
   konigu siajn ŝanĝojn al la komunumo, por ke ĉiuj uzantoj povu profiti
   de iliaj plibonigoj.

Rajtocedaj programoj povas esti venditaj, kiel ĉiuj aliaj liberaj
programoj, sed postuli, ke komercaj plibonigoj restu liberaj certigas,
ke la kvar liberecoj estu respektataj de ĉiuj partoprenantoj. Krome
estas malfacile ŝanĝi la licencon de rajtoceda programo, se la
aŭtorrajto apartenas [al kelkaj homoj][0], kio utilas kiel forta promeso,
ke la programo restos libera en la estonteco.

[0]: ../participate/copyright-ownership/

## Molaj kaj fortaj licencoj

Rajtocedaj permesiloj malsamas pri kiom forte iliaj rajtocedaj
paragrafoj influas la reuzon de la programo. Ekzemple la mola rajtoceda
[Mozilla Public License][MPL] estas *bazita sur dosieroj*, tiel ke la
rajtoceda paragrafo kovras individuajn fontkoddosierojn, kaj ne la
projekton kiel tuton: vi povas meti unu el ĉi tiuj dosieroj en ajnan
projekton, sen ke vi devu relicenci la pli grandan projekton, kondiĉe ke
vi republikigas ajnajn ŝanĝojn de tiuj specifaj dosieroj.

[MPL]: https://www.mozilla.org/en-US/MPL/2.0/

Iom pli forta ekzemplo estas la [GNU Lesser General Public License][LGPL],
kiu specife traktas programajn bibliotekojn. Ĉi tiuj bibliotekoj estas
kompilitaj en artefakton de programaro, kiel komuna objekto aŭ
statika dosiero, kaj la rajtocedaj kondiĉoj validas por ĉi tiu tuta
artefakto. Tamen kiam ĉi tio estas ligita kun programo de ekstera
liveranto, la rajtoceda paragrafo ne validas. Eĉ pli forta estas
la [GNU General Public License][GPL], kiu traktas la kompletigitan
programon kiel la programan artifakton, al kiu la rajtoceda paragrafo
aplikas.

[LGPL]: https://www.gnu.org/licenses/lgpl-3.0.en.html
[GPL]: https://www.gnu.org/licenses/gpl-3.0.html

Ĉe la fino de la rajtoceda spektro estas permesiloj kiel la [GNU Affero
General Public License][AGPL], kiu etendas la
<abbr title="GNU General Public License">GPL</abbr> por apliki ĝin al
programoj uzataj tra reto, kiel datumbazoj, kaj konsideras la uzantojn
de tiu programo kiel «ricevantojn» de la programo, kiuj estas do
rajtigitaj ricevi la fontkodon.

[AGPL]: https://www.gnu.org/licenses/agpl-3.0.html

## Kiel reuzi rajtocedajn verkojn

La plej simpla maniero reuzi rajtocedajn verkojn estas apliki iliajn
permesilojn al via propra laboro kaj konforme distribui ĝin.

Se vi ne volas fari tion, vi povas nur uzi rajtocedan verkon sub la
kondiĉoj permesataj de ĝia licenco, kaj vi verŝajne estos limigita al la
uzo de molaj rajtocedaj verkoj. Ekzemple se via programo dependas de
biblioteko, kiu uzas la <abbr title="GNU Lesser General Public License">LGPL</abbr>,
vi povos uzi ĉiun ajn licencon por via propra laboro, sed vi devos
kunhavigi la ŝanĝojn, kiujn vi faras al la biblioteko mem. Se la
programo uzas la GPL- aŭ AGPL-permesiloj, vi estos pli limigita. Legu
zorge la licencajn kondiĉojn kaj konsultu advokaton, se vi ne certas
kiel procedi.

Por pli da detaloj konsultu nian paĝon pri [reuzi liberajn
programojn](../partopreni/derivitaj-verkoj/).

{{< tip >}}
[Software Freedom Conservancy][sfc] estas organizo, kiu, inter aliaj
agoj, uzas jurajn rimedojn por rajtoceda observigo. Por lerni pli pri
rajtoceda observigo por viaj propraj projektoj, konsultu iliajn rimedojn.

[sfc]: https://sfconservancy.org/
{{< /tip >}}
