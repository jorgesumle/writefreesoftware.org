---
title: La kvar liberecoj
weight: -10
slug: kvar-liberecoj
---

<blockquote>
  <ol start="0">
    <li>
      La libereco <strong>uzi</strong> la programon por ajna celo.
    </li>
    <li>
      La libereco <strong>studi</strong> kaj <strong>plibonigi</strong>
      la programon.
    </li>
    <li>
      La libereco <strong>kunhavigi</strong> la programon.
    </li>
    <li>
      La libereco <strong>kunlabori</strong> por disvolvi la programon.
    </li>
  </ol>
</blockquote>

Ni ekzamenu ĉiujn el la kvar liberecoj pli detale.

## 0: Uzi la programon

La «0-a» libereco garantias la rajton, ke ĉiu povu **uzi** la programon
por ajna celo. Vi estas rajtigita uzi ajnan programon por ĉiu ajn celo,
inkludante komercan uzon --- kontraŭintuicie vi povas vendi liberajn
programojn. Vi ankaŭ povas inkludi liberajn programojn en vian propran
verkon, sed estu singarda --- estas iuj eblaj problemoj, pri kiuj ni
parolos en [Reuzi liberajn programojn](../partopreni/derivitaj-verkoj/).

{{< tip >}}
En la lingvaĵo de liberaj programoj ĉi tio estas ofte nomita
«nediskriminacia» postulo.
{{< /tip >}}

## 1: Studi kaj plibonigi la programon

La unua libereco garantias la rajton **studi** la programan agmanieron.
Vi havas la rajton kompreni kiel la programo, sur kiu dependas,
funkcias! Por faciligi ĉi tion la fontkodo devas esti inkludita ĉe la
programo. La eldonantoj de libera programo ne povas malklarigi la
fontkodon, malpermesi retroprojektadon aŭ alie eviti, ke vi uzu ĝin.

Aldone, vi havas rajton **plibonigi** la fontkodon. Oni ne nur donis al
vi la rajton legi la fontkodon de libera programo, sed ankaŭ la rajton
ŝanĝi ĝin por pli bone adapti ĝin al viaj bezonoj.

## 2: Kunhavigi la programon

La dua libereco garantias la rajton **kunhavigi** liberajn programojn
kun aliaj. Se vi havas kopion de iu libera programo, vi povas doni ĝin
al viaj amikoj, publikigi ĝin en via retejo aŭ paki ĝin kun alia
programo kaj kunhavigi ĝin kiel parton de io pli granda. Vi ankaŭ povas
kunhavigi viajn plibonigojn de ĝi --- tiel pli bonas por ĉiuj!

{{< tip >}}
Vi povas pagigi al tiuj, kun kiuj vi kunhavigas la programon, ekzemple
por kovri la bendolarĝan koston. Memoru tamen, ke la ricevanto rajtas
kunhavigi ĝin mem, sed repagi neniun tantiemon.
{{< /tip >}}

## 3: Kunlabori por disvolvi la programon

La tria libereco garantias unu plian rajton: la rajton **kunlabori** kun
aliaj plibonigante la programon. Vi povas studi, plibonigi kaj kunhavigi
la fontkodon, kaj la homoj, kun kiuj vi kunhavigas ĝin, povas studi,
plibonigi kaj denove kunhavigi ĝin kun vi. Ĉi tio estas la fundamento de
la **movado por liberaj programoj**: tutmonda komunumo de entuziasmuloj,
kiuj kune kunhavigas kaj plibonigas programojn.

{{< button "../permesiloj/" "Sekvas: Permesiloj de liberaj programoj" "next-button" >}}
