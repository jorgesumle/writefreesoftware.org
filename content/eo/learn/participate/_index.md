---
title: Partopreni
weight: 2
url: lerni/partopreni
---

Estas multaj manieroj partopreni en la movado por la liberaj programoj,
kiuj inkludas publikigi viajn proprajn liberprogramajn projektojn,
kontribui al ekzistantaj liberprogramaj komunumoj, organizi eventojn kaj
aktivismon por la kaŭzo de la liberaj programoj kaj pli.

{{< button "kontribui/" "Lernu pli pri kontribui" >}}{{< button "publikigi/" "Lernu pli pri publikigi" >}}
