---
title: Publikigi liberajn programojn
weight: 2
url: lerni/partopreni/publikigi
---

Ĉu vi havas bonegan ideon por nova liberprograma projekto kaj volegas
publikigi ĝin? Bonege! Jen la paŝoj:

1. Skribu tion!
2. Elektu programan permesilon
3. Publikigu vian projekton
4. Kreu komunumon (laŭvole)

## Skribi vian programon

Ni ne povas helpi vin multe kun ĉi tiu parto, kompreneble. Tamen jen
konsilo: **publikigu baldaŭ**. Multaj funkctenantoj hezitas publikigi
sian kodon, zorgitaj pri malaltkvalita dokumentaro, mankantaj funkcioj,
abundaj eraroj aŭ eĉ nur malaltkvalita kodo --- ĉi ĉio devas esti
riparita antaŭ ol ĝi pretas por la mondo. Ne tiel! Publikigi baldaŭ
estas decida paŝo por rapide ricevi komentojn kaj allogi la helpon de
aliuloj. Eldonu vian kodon kiel eble plej baldaŭ.

## Elekti permesilon

Antaŭ ol publikigi tamen vi devas elekti liberprograman permesilon, kiu
taŭgas por viaj bezonoj. Vi mem *povas* skribi unu, sed tio estas **ege**
malrekomendita, eĉ por grandaj firmaoj, kiuj povas dungi helpantan
advokaton. Estas multege da permesiloj, kiuj taŭgas por ĉiuj bezonoj,
kaj ni analizas kiel elekti la ĝustan por vi en [Elekti permesilon][0].

[0]: ../elekti-permesilon/

Elekti permesilon **devigas**, kiam vi publikigas liberprograman
projekton --- se permesilo mankas, via verko estas ne-libera. Via elekto
ankaŭ povus estigi longtempajn sekvojn por via projekto, do zorge
elektu. Ĉi tio estas grava paŝo.

## Publikigi vian projekton

Kiam vi pretas publikigi vian verkon, la plej facila metodo estas
alŝuti ĝin al **kodejo**. Eblas elekti multajn, ĉiuj provizas multajn
ilojn por faciligi al vi la disvolvadon kaj kunlaboradon. Multaj el ĉi
tiuj kodejoj estas mem liberaj programoj --- ni rekomendas uzi ĉi tiajn
anstataŭ ne-liberaj alternativoj. Jen estas kelkaj rigardindaj sugestoj:

- [Codeberg](https://codeberg.org)
- [SourceHut](https://sourcehut.org)

Vi ankaŭ povas gastigi vian projekton en via propra infrastrukturo. Io
tiel simpla kiel tar-dosiero en via servilo sufiĉos, sed vi ankaŭ povas
ruli tutan kodejon en via propra datumejo, se tio pli bonas por via
projekto.

<!--

Feel free to add forges here. Criteria for inclusion:

- Reasonably feature complete and useful for hosting projects with minimal fuss
- Hosted on stable infrastructure (self-hosted forges will not be considered)
- Trustworthy and transparent maintainership
- Free software, of course

Add new hosts in alphabetical order.

-->

## Krei komunumon

Unu el la ĉefaj avantaĝoj publikigi liberajn programojn estas, ke via
komunumo povas engaĝiĝi helpante igi ĝin pli bona ol vi povas fari sole.
Administri vian komunumon gravas por certigi, ke via programo estas kiel
eble plej bona. [Ni publikigas artikolojn][blogo] pri diversaj temoj
interesaj por partoprenantoj en liberaj programoj, inkludante konsilojn
pri kreado kaj administrado de komunumoj. Vizitu ĝin!

[blogo]: ../../../blogo/
