---
title: Kontribui al ĉi tiu retejo
weight: 1000
url: lerni/partopreni/kontribui-al-ĉi-tiu-retejo
---

Ĉi tiu retejo mem estas libera programo. La fontkodo de la enhavo kaj la
aranĝo disponeblas [en Codeberg][0], liberprograma disvolvada platformo.
Sekvu la ligilon por akiri pli da informo pri kiel kontribui
plibonigojn! La retejo kaj ĝia enhavo uzas la permesilon Creative Commons
Atribuite-Samkondiĉe 4.0 Tutmonda ([CC BY-SA 4.0][CC-BY-SA]), rajtocedan
permesilon.

[0]: https://codeberg.org/jorgesumle/writefreesoftware.org
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/deed.eo

La etoso baziĝas sur la Hugo-etoso
<a href="https://github.com/onweru/compose" class="non-free" title="Averto: ĉi tiu ligilo kondukos vin al GitHub, ne-libera retejo">compose</a>, kiu
estas libera programo licencita per la [MIT-permesilo](https://mit-license.org/), aŭtorrajto &copy; 2020 Weru.
