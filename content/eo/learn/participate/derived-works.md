---
title: Reuzi liberajn programojn
weight: 90
url: lerni/partopreni/derivitaj-verkoj
---

Unu el la grandaj avantaĝoj de liberaj programoj estas ilia kapablo esti
reuzitaj. Vi povas integri kodon el aliaj programaj projektoj en novajn
projektojn, ŝparante tempon kaj permesante al vi konstrui sur la ŝultroj
de gigantoj. Kompreneble vi devas respekti la laboron de la origina
projekto, kaj tio signifas observi la kondiĉojn de ĝia liberprograma
permesilo.

{{< tip "warning" >}}
Ĉiam legu zorge la permesilon, kiam vi integras la laboron de aliulo en
vian propran programon.
{{< /tip >}}

## Integri permeseman programon en novajn verkojn

La ĉefa allogo de permesemaj programaj permesiloj estas la eblo integri
ilin en ĉion ajn kun relative malmulte da devigo rilate al la aŭtorajtaj
posedantoj. La plejparto da la permesemaj permesiloj nur postulas, ke vi
inkludu la permesilan tekston, aŭ eĉ nur rajtoceda deklaro, en via
projekto. Por liberprogramaj projektoj, kiuj integras permeseme
licencitaj verkoj en sian laboron, plenumi tiujn devigojn ofte estas
tiel simpla kiel inkludi kroman permesilon kun via fontkodo.

Kiam vi integras permeseme licencitan liberan programon en ne-liberajn
verkojn, vi devas distribui la liberprograman permesilon kaj/aŭ
rajtocedan atribuon kun via programo. Multaj komercaj uzantoj de
permeseme licencitaj liberaj programoj inkludas menuon ie en siaj
produktoj, kiu listigas la aplikeblajn programajn permesilojn aŭ
inkludas presitan version kun la produkto. Vi devos elpensi similan metodon.

## Integri rajtocedan programon en novajn verkojn

{{< tip "warning" >}}
Rajtoceda programon **ne povas** esti integrita en ne-liberan programon.
{{< /tip >}}

La plejparto de liberaj programoj povas esti integrita en rajtocedajn
programojn, kaj inverse, se la permesiloj **kongruas**. Ĝenerale la plej
popularaj permesemaj permesiloj --- sed ne ĉiuj --- kongruas kun la plej
popularaj rajtocedaj permesiloj. *Iuj* rajtocedaj permesiloj kongruas
kun aliaj rajtocedaj permesiloj (ekzemple la Mozilla Public License 2.0
kongruas kun la GNU-familio de permesiloj), sed multaj ne. Du ajnaj
projektoj, kiuj uzas la *saman* rajtocedan permesilon kongruas inter si
kaj povas libere kunhavigi kodon.

{{< tip >}}
GNU gardas liston de permesiloj, kiuj estas kongruaj kaj nekongruaj
kun la GPL-familio de rajtocedaj permesiloj, [ĉi tie][0].

[0]: https://www.gnu.org/licenses/license-list.html
{{< /tip >}}

Integri permeseman kodon en rajtocedan projekton simplas, se la
permesiloj kongruas: vidu la antaŭa sekcio.

{{< tip "warning" >}}
La inverso, integri rajtocedan programon en permeseme licencitan
programan projekton, estas malpli simpla. En ĉi tiu okazo, la kombinita
laboro iĝas submetita al la kondiĉoj de la rajtoceda permesilo.
Administri kombinaĵon de permesemaj kaj rajtocedaj permesiloj en sola
verko eblas, sed havas gravajn kaj signifajn sekvojn por via projekto.
Ĉi tio estas forte malrekomendita al ne-spertuloj: ne miksu rajtocedan
kodon kun permesema projekto, krom se vi estas preparita, por ke la
projekto
[ŝanĝiĝu al rajtoceda permesilo](../rajtoceda-posedo/#ŝanĝi-projektan-permesilon).
{{< /tip >}}

## Administri multajn permesilojn kaj aŭtorrajtojn en unu projekto

Pli grandaj kaj kompleksaj projektoj povas integri programojn el multaj
diversaj fontoj kun multaj malsamaj permesiloj kaj aŭtorrajtaj
posedantoj. Se ĉi tio priskribas vian projekton, ni rekomendas, ke vi
konsideru apliki la
[REUSE](https://reuse.software/)-normon al via laboro.
