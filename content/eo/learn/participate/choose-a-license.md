---
title: Elekti permesilon
weight: 3
url: lerni/partopreni/elekti-permesilon
---

Elekti permesilon estas grava parto de publikigo de viaj liberprogramaj
projektoj. Estas multaj elektoj kaj ĉiuj havas malsamajn avantaĝojn kaj
sekvojn por la estonteco de via projekto. Povas malfacili [ŝanĝi la
permesilon poste], do vi devos konsideri tion zorge en la komenco.

[ŝanĝi la permesilon poste]: ../rajtoceda-posedo/#ŝanĝi-projektan-permesilon

Jen estas kelkaj multe uzitaj liberprogramaj permesiloj, kiujn ni
rekomendas, kaj kial elekti ilin.

{{< tip >}}
Post kiam vi elektas permesilon, inkludu ĝin, kiam vi kunhavigas vian
programon. La plej facila maniero fari tion estas kopii la
platan-tekstan version en dosieron nomitan «COPYING» en via fontkoda
deponejo. Por pli kompleksaj situacioj ni rekomendas la [REUSE][0]
metodo.

[0]: https://reuse.software/
{{< /tip >}}

## Rajtocedaj permesiloj

Rajtocedaj permesiloj utilas por garantii, ke via programo restas
libera. La uzo de la rajtocedo postulas al iu ajn, kiu plibonigas vian
programon, publikigi la plibonigojn sub la sama rajtoceda permesilo, kiu
garantias, ke vi povu integri riajn plibonigojn denove en vian version.
Por pli da detaloj vidu
[Kio estas rajtocedo?](../../rajtocedo/)

{{< block "grid-2" >}}

{{< column "pros" >}}

### Avantaĝoj

* Certigas, ke via programo restos libera
* Instigas komunumajn kontribuojn
* Ĝenerale antaŭenigas liberajn programojn

{{< /column >}}

{{< column "cons" >}}

### Malavantaĝoj

* Malpli allogaj por firmaoj
* Devas konsideri licenckongruecon por reuzo

{{< /column >}}

{{< /block >}}

### Rekomendataj rajtocedaj permesiloj

| Permesilo | Uzu ĝin por... | Rajtoceda metodo |
| --- | --- | --- |
| [Mozilla Public License 2.0] | Bibliotekoj (permesas <i lang="en"><abbr title="La ago kopii bibliotekoajn dosierojn rekte al alia projekto anstataŭ aparte ligi al ili">vendoring</abbr></i>) | Bazita sur dosieroj |
| [GNU Lesser General Public License] | Bibliotekoj (malpermesas <i lang="en">vendoring</i>) | Bazita sur objektoj |
| [GNU General Public License] | Plenumeblaj programoj | Bazita sur plenumeblaĵoj |
| [GNU Affero General Public License] | Retaj servoj | Bazita sur reto |

[Mozilla Public License 2.0]: https://www.mozilla.org/en-US/MPL/2.0/
[GNU Lesser General Public License]: https://www.gnu.org/licenses/lgpl-3.0.en.html
[GNU General Public License]: https://www.gnu.org/licenses/gpl-3.0.html
[GNU Affero General Public License]: https://www.gnu.org/licenses/agpl-3.0.html

## Permesemaj permesiloj

Permesemaj permesiloj trudas relative malmultajn devigojn al la ricevanto de
via programo. Ĉi tiaj permesiloj permesas, ke la programo estu libere
reuzita kaj integrita en iun ajn alian programan projekton, inkludante
ne-liberan programon. Tio povas utili por projektoj kies celo estas
komerca uzo aŭ vasta adopto.

{{< block "grid-2" >}}

{{< column "pros" >}}

### Avantaĝoj

* Ebligas facilan reuzon
* Instigas vastan adopton
* Alloga por komercaj uzantoj

{{< /column >}}

{{< column "cons" >}}

### Malavantaĝoj

* Povas esti enigita en ne-liberajn verkojn
* Malinstigas komunumajn kontribuojn

{{< /column >}}

{{< /block >}}

### Rekomendataj permesemaj permesiloj

Ni rekomendas la sekvajn permesemajn permesilojn:

* [MIT-permesilo](https://mit-license.org/)
* [BSD 3-clause permesilo](https://opensource.org/license/bsd-3-clause/)

## Rekomendataj por firmaoj

Por firmaoj, kiuj eldonas liberajn programojn, eble estas dezirinde uzi
permesema permesilo, kiu inkludas konsiderojn por varmarkoj kaj patentaj
rajtoj. Por ĉi tiu celo ni rekomendas la [Apache 2.0-permesilon].

[Apache 2.0-permesilon]: https://www.apache.org/licenses/LICENSE-2.0.html

## Publika havaĵo

Eldonistoj, kiuj deziras meti siajn programojn en la publikan havaĵon,
devus scii, ke simpla dediĉo al la publika havaĵo ne sufiĉas por
internacia uzo. Ni rekomendas la sekvajn permesilojn, kiuj provizas
jurajn rajtojn ekvivalentajn al la publika havaĵo per maniero kongrua
kun internaciaj leĝoj:

* [Creative Commons 0](https://creativecommons.org/share-your-work/public-domain/cc0/)
* [Unlicense](https://unlicense.org/)

## Permesiloj por aliaj situacioj

Ni havas apartan paĝon rekomendantan permesilojn por ne-programaj
havaĵoj, kiel aŭdvidaĵoj:

[Licenci ne-programajn havaĵojn](../havaĵoj/)
