---
title: Licenci neprogramajn havaĵojn
weight: 10
url: lerni/partopreni/havaĵoj
---

Liberprogramaj permesiloj estas la plej taŭgaj por licenci programojn.
Tamen liberprogramaj projektoj ofte inkludi aŭdvidaĵojn, kiuj ne estas
per si mem programoj, kiel artaĵoj kaj dokumentaro. Malsamaj permesiloj
estas rekomenditaj por ĉi tiuj uzsituacioj. Ni havas kelkajn rekomendojn
por permesiloj, kiuj taŭgas por ne-programaj aŭdvidaĵoj, kunhavas la
spiriton de la liberaj programoj kaj kongruas kun liberprogramaj
permesiloj.

## Creative Commons

Por la plejmulto de plurmediaj havaĵoj --- bildoj, aŭdaĵoj, videoj,
skribaĵoj kaj tiel plu --- taŭgas uzi [Creative Commons][0]-permesilojn,
kiu inkludas agordeblajn opciojn por apartaĵoj kiel rajtocedo kaj
atribuo. Rimarku tamen, ke la -ND (nemodifite) kaj -NC (nekomerce)
specoj de la Creative Commons permesiloj estas nekongruaj kun liberaj
programoj kaj la uzo de ĉi tiaj havaĵoj limigos la utilecon de via
projekto ene de la medio de la liberaj programoj.

[0]: https://creativecommons.org/

## Aparataro

Aparataraj projektoj (skemoj, HDL-fontoj, ktp.) estas instigitaj uzi la
<a href="https://cern-ohl.web.cern.ch/home" class="non-free" title="Ĉi tiu ligilo kondukos vin al ne-libera retejo">CERN Open Hardware License</a>.

## Tiparoj

La
<a href="https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL" class="non-free" title="Ĉi tiu ligilo kondukos vin al ne-libera retejo">SIL Open Font License</a>
estas rekomendata por distribui fontojn per maniero kongrua kun liberaj
programoj.

## Dokumentaro

La plejmulto da projektoj ne uzas specialan permesilon por sia
dokumentaro. Tamen la [GNU Free Documentation License][fdl] estas
kelkfoje uzita por ĉi tiu intenco.

[fdl]: https://www.gnu.org/licenses/fdl-1.3.html
