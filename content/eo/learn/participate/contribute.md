---
title: Kontribui al liberaj programoj
weight: 1
url: lerni/partopreni/kontribui
---

Multaj projektoj de liberaj programoj serĉas helpon. Sendepende de via
kompetenteca fako --- kompetenteco pri iu programlingvo, skribante
dokumentaron, komunuma administrado, merkatigkapabloj, fasonada kaj
arta kapabloj --- ekzistas projekto, kiu povus profiti de via helpo.
Rigardu la ilojn, kiujn vi uzas ĉiutage: verŝajne multaj el ili jam
estas liberaj programoj.

Iliaj komunumoj atendas renkonti vin! Trovu iliajn babilejojn,
interretajn diskutejojn kaj aliajn renkontejojn kaj prezentu vin.

## Sciindaj aferoj

Kiam vi sendas plibonigojn al liberaj programoj, oni kutime atendos, ke
vi donu ilin sub la kondiĉoj de la programa permesilo, per kiu la
projekto estas distribuita. Inspektu la permesilon kaj garantiu, ke vi
povas konsenti kun ĝiaj kondiĉoj, kiam vi donas permesilon al viaj
propraj kontribuoj.

Se vi ne posedas la verkon, al kiu vi kontribuas (ekzemple vi kontribuas
per la tempo aŭ aparataro de via dunganto), vi devas akiri la rajtigon
de la aŭtorrajta posedanto. Ĉi tio ankaŭ validas, se vi kunmetas kodon
de alia liberprograma projekto --- vidu <nobr>[reuzi liberajn programojn][0]</nobr>
por pli da detaloj.

[0]: ../derivitaj-verkoj/

## Pri «permesila konsento de kontribuanto»

Iuj projektoj petos, ke vi subskribu «permesilan konsenton de
kontribuanto», aŭ similan dokumenton, kiam vi donas vian kontribuon.
Gravas, ke vi zorge legu ĝin. Ĉi tiu dokumento povus esti uzita simple
por kontroli, ke vi posedas la aŭtorrajton sur via ŝanĝo kaj havas la
rajton kontribui ĝin. Tamen multaj el ĉi tiuj konsentoj ankaŭ povus
peti, ke vi rezignu iujn el viaj rajtoj, ekzemple por permesi al la
eldonisto inkludi viajn ŝanĝojn al novaj versioj de la programo.

Vi ne estas devigita rezigni viajn rajtojn. Tiaj akordoj povus esti
bezonataj, por ke la originala eldonisto akceptu inkludi viajn ŝanĝojn
al sia versio de la programo kaj distribuu tiujn ŝanĝojn por vi. Tamen
vi ĉiam rajtas mem distribui plibonigitan version de la programo,
sendepende de la originala eldonisto.

Eldonistoj de liberaj programoj estas multe malinstigitaj uzi
permesilajn konsentojn de kontribuantoj por administri siajn komunumajn
kontribuojn. Por pli da detaloj vidu
[Administri rajtocedan posedon][1].

[1]: ../rajtoceda-posedo/
