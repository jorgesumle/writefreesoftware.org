---
title: Administri rajtocedan posedon
weight: 90
url: lerni/partopreni/rajtoceda-posedo
---

Indas klarigi la naturon de la aŭtorrajta posedo en liberaj programoj.
Se mankas permesila konsento de kontribuanto aŭ simila dokumento (ago,
kiun ni [ege malrekomendas][0]), kiel disvolvantoj kaj eldonistoj de
liberaj programoj administras la leĝajn rajtojn ligitajn kun la programa
aŭtorrajto?

[0]: ../kontribui/#pri-permesila-konsento-de-kontribuanto

## Kiu posedas liberprograma projekto?

Kiam vi kontribuas al liberprograma projekto, se vi ne asignas vian
aŭtorrajton al iu alia, vi retenos la posedon de la rajtoj de la
intelekta propraĵo ligitaj al viaj kontribuoj. Viaj ŝanĝoj estas tiam
*licencitaj* al ĉiuj aliaj, inkludante la aliajn aŭtorrajtajn posedantojn,
sub la samaj kondiĉoj kiel la permesilo, per kiu la projekto komence
estis distribuita.

Tiel, preskaŭ ĉiam, la aŭtorrajtoj de ĉiu ajn programa projekto estas
tenata sume de ĉiuj homoj, kiuj kontribuis intelektan havaĵon al ĝi,
kiuj licencas ĝin al uzantoj, kaj inter si, per [liberprograma
permesilo][1].

[1]: ../../permesiloj/

## Determini devenon

Povus esti dezirinde por iuj projektoj, precipe komercaj projektoj,
formale determini jenon por ĉiu kontribuo:

1. La kontribuinto posedas la aŭtorrajton de sia kontribuo aŭ estas
   rajtigita de la aŭtorrajta posedanto uzi ĝin.
2. La kontribuinto akceptas licenci sian aŭtorrajton sub la kondiĉoj de
   la projekta permesilo.

Se estas dezirinde por via projekto formale determini la devenon per ĉi
tiu maniero, ni rekomendas uzi la [Developer Certificate of Origin][2].
La plejparto da projektoj faciligas tion demandante al aŭtoroj
«subskribi» iliajn kontribuojn. La versikontrola sistemo [Git][3]
provizas manieron montri, ke iu difinita kontribuo estis subskribita per
la opcio [`git commit -s`][4].

[2]: https://developercertificate.org/
[3]: https://git-scm.com/
[4]: https://git-scm.com/docs/git-commit#Documentation/git-commit.txt--s

## Ŝanĝi projektan permesilon

Eble vi iam volos ŝanĝi la permesilon de via projekto.

[permesema permesilo]: ../elekti-permesilon/#permesemaj-permesiloj
[rajtoceda permesilo]: ../elekti-permesilon/#rajtocedaj-permesiloj

Se la projekto estas licencita per [permesema permesilo], ĝenerale eblas
sublicenci la originan projekton per nova permesilo kaj apliki la novan
permesilon al estontaj ŝanĝoj. Vi ankoraŭ devos observi la originajn
permesilkondiĉojn, kiel atribuon, sed estontaj ŝanĝoj devos esti
licencitaj sub malsamaj kondiĉoj. Laŭ ĉi tiu senco ŝanĝi la permesilon
similas al komenci novan projekton kaj inkludi la originan kodon en ĝin.

Tamen se la projekto estas licencita per [rajtoceda permesilo], tio pli
malfacilas --- ofte maleblas. Ĉi tio estas intenca eco de rajtocedaj
permesiloj: necesas por eviti, ke la projekto estu integrita en
ne-liberan programon. Vi ĝenerale ne povas sublicenci rajtocedan
projekton same kiel vi licencas permeseman projekton.

Eblas ŝanĝi la permesilon de rajtoceda projekto, sed vi devas aŭ
<nobr>**(a)** akiri la</nobr> permeson de ĉiuj aŭtorrajtaj posedantoj aŭ
<nobr>**(b)** reskribi iliajn kontribuojn</nobr>. Ĉi tio metodo ankaŭ
taŭgas, se vi volas ŝanĝi permeseman permesilon sen esti submetita al
ĝiaj originaj kondiĉoj (kiel atribuo), sed ĉi tio kutime ne valoras la
penon konsiderante la relative ne-striktaj kondiĉoj de permesemaj
permesiloj.

Estas pro tio, ke teni la aŭtorrajton de la kontribuintoj sume,
anstataŭ asigni aŭtorrajton al sola estaĵo, estas rekomendata por
rajtocedaj projektoj: tio fortigas la longtempan sekurecon de la
projekta liberprograma stato igante, ke estu pli malfacile ŝanĝu ĝin al
ne-libera permesilo.
