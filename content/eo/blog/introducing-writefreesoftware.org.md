+++
author = "Drew DeVault"
title = "Skribu liberajn programojn: kompleta edukada rimedo por la liberprograma movado"
date = "2023-05-17"
description = "Ĉi tio estas nova retejo por disvastigi la filozofion kaj la praktikon de la liberaj programoj"
tags = ["meta"]
image = "banner-blog.webp"
+++

Hodiaŭ mi ĝojas anonci la publikigon de
[Skribu liberajn programojn](https://freakspot.net/escribe-programas-libres/eo/)!
Ĉi tiu retejo estas nova ilo por la liberprograma komunumo, kiu celas
provizi ampleksan kaj alireblan enkondukon kaj referencon por la
liberprograma movado. Ni celas eduki aliulojn pri la liberprograma
filozofio kaj praktiko, helpi al homoj kompreni kiel liberaj programoj
gravas kaj kiel apliki tion.

Venu ĉi tien por lerni aferojn kiel:

- [Kio estas liberaj programoj](https://freakspot.net/escribe-programas-libres/eo/lerni/)
- [Kiel liberprogramaj permesiloj funkcias?](https://freakspot.net/escribe-programas-libres/eo/lerni/permesiloj/)
- [Kiel la rajtocedo funkcias?](https://freakspot.net/escribe-programas-libres/eo/lerni/rajtocedo/)
- [Kiel elekti liberprograman permesilon](https://freakspot.net/escribe-programas-libres/eo/lerni/partopreni/elekti-permesilon/)
- [Kiel reuzi liberajn programojn](https://freakspot.net/escribe-programas-libres/eo/lerni/partopreni/derivitaj-verkoj/)

Kaj [multe pli](https://freakspot.net/escribe-programas-libres/eo/lerni/)!

La ĉefa enhavo de <cite>Skribu liberajn programojn</cite> provizas multege da utilaj
informaj rimedoj pri kiel liberaj programoj funkcias kaj kiel iliaj
principoj povas esti aplikitaj, sed tio ne estas ĉio pri liberaj
programoj. La [blogo][1], kvankam ĝi nur komenciĝas, donas multe pli da
liberaj rimedoj por helpi al funkctenantoj kaj kontribuantoj pli bone
kompreni kiel efike labori per liberaj programoj.

Estontaj temoj por la blogo inkludas:

[1]: ..

- Krei kaj administri viajn liberprogramajn komunumojn
- Analizo de specifaj liberprogramaj permesiloj
- Komerca partopreno en liberaj programoj
- La rajtoceda graveco praktike

Ĉu vi volas helpi pri ĉi tio aŭ alia ideo?
[Konsideru skribi por ni!](https://codeberg.org/jorgesumle/writefreesoftware.org)
