+++
author = "Drew DeVault"
title = "La kompleta gvidilo por publikigi liberajn videludojn"
date = "2021-03-23"
description = "Kompleta gvidilo por publikigi vian ludon kiel liberan programon"
tags = ["ludoj", "distribuidores"]
image = "supertuxkart.jpg"
+++

*Ĉi tiu artikolo estis tradukita kaj adaptita el la [blogo de Drew DeVault](https://drewdevault.com/2021/03/23/Open-sourcing-video-games.html).*

Videludoj estas interesa klaso de programo. Male ol la plejmulto da
programoj, ili estas kreema peno, anstataŭ praktika utilaĵo. Dum la
plejparto da programoj postulas novajn funkciojn por respondi al la
praktikaj bezonoj de siaj uzantoj, videludoj postulas novajn funkciojn
por servi la kreeman vizion de siaj kreantoj. Simile, aferoj kiel
restrukturigado kaj pago de la teknika ŝuldo ofte estas multe
malprioritigitaj por prefere eldoni ion kiel eble plej baldaŭ. Multaj el
la kunlaboraj avantaĝoj de la liberaj programoj estas malpli aplikeblaj
al la videludoj. Eble pro ĉi tiuj kialoj estas tre malmultaj komercaj
liberaj ludoj.

Tamen estas kelkaj ekzemploj de ĉi tiaj ludoj, kaj ili havis grandan
influon sur la videludado. Id estas fama pro tio, publikigis la
fontkodon de pluraj versioj de <cite>DOOM</cite>. La motoro Quake ankaŭ
estis publikigita sub la GPL-permesilo kaj iĝis tre influa, servinte
kiel bazo por multaj ludoj, inter ili klazikaĵo kiel la serio
<cite>Half Life</cite>. Granda parto de la kanono de la videludoj eblis
danke al la malavaraj kontribuoj de la publikigantoj de liberaj
programoj.

Publikigi liberajn ludojn estas ankaŭ estas afero de historia
konservado. La proprietaj ludoj tendencas atrofiiĝi. Multe post iliaj
glortempoj, kun malabundaj taŭgaj platformoj kaj fizikaj kopioj
malfacile akireblaj, multaj ludoj mortas malrapide kaj silente,
forgesitaj en la historiaj analoj. Iuj ludoj superis ĉi tion liberigante
sian fontkodon, faciligante al ŝatantoj porti la ludon al novaj
platformoj kaj konservi ĝin viva.

Kio estos la heredaĵo de via ludo? Ĉu ĝi estos tute forgesita, nekapabla
funkcii sur nuntempaj platformoj? Ĉu ĝia kodo estos disponebla, foje
utila por la dediĉita ludanto, sed kun malmulte da plua atingo? Eble ĝi
sekvas la vojon de <cite>DOOM</cite>, eterne vivante en adaptoj al
centoj da aparatoj kaj operaciumoj. Eble ĝi sekvos la vojon de <cite>Quake</cite>
kaj ĝia animo ĉiam apartenos al la estontaj klasikaĵoj. Se vi tenas la
fontkodon fermita, la sola rezulto estas la unua: iam ĝojita, nun
forgesita.

Kondiserante ĉi tion, kiel vi sekurigas la heredaĵon de via ludo?

## Disponebla kodo: la minimumo

La minimumo estas igi vian ludon «koddisponebla». Sciu, ke tio ne estas
la sama afero kiel igi ĝin libera aŭ malfermitkoda! Iuj ekzemploj de ĉi
tiu kategorio inkludas
<cite>Alien 3</cite>,
<cite>Civilization IV</cite> kaj <cite>V</cite>, <cite>Crysis</cite>, <cite>Deus Ex</cite>, <cite>Prince of Persia</cite>,
<cite>Unreal Tournament</cite> kaj <cite>VVVVVV</cite>.

Ĉi tiu metodo igas, ke oni povu vidi vian fontkodon kaj eble kompili kaj
ruli ĝin, sed malpermesas la derivitajn verkojn. Ĉi tio certe pli bonas
ol lasi ĝin malfermitkoda: donas utilajn rimedojn por modifantoj,
rapidludantoj kaj aliaj fanoj; kaj sindonaj ludantoj povas uzi ĝin kiel
bazon por funkciigi la ludon en estontaj platformoj, kvankam sole kaj
sen la rajto kunhavigi sian laboron.

Se vi elektus metodon de minimuma limigo, iuj ludantoj povus kunhavigi
sian laboron, sed vi lasus al ili en manka jura situacio. Mi rekomendus
tion, se vi estas tre protektema de via intelekta propraĵo, sed sciu, ke
vi limigos la eblan duan vivon de via ludo, se vi elektas ĉi tiu
metodon.

## Rajtocedo kun proprietaj havaĵoj

La sekva paŝo estas liberigi vian programon per rajtoceda permesilo, sed
evitante etendi la permesilon al la havaĵoj &mdash; iu ajn kiu volas
funkciigi la fontkodon devus aŭ aĉeti la ludon el vi kaj eltiri la
havaĵojn aŭ provizi siajn proprajn havaĵojn komunume kreitajn. Ĉi tio
estas populara metodo inter liberprogramaj ludoj kaj donas al vi la
plejmulton da la avantaĝoj kaj malmulton da malavantaĝoj. Vi aliĝos al
niaj ekzemploj de <cite>DOOM</cite>, <cite>Quake</cite>, <cite>Amnesia:
The Dark Descent</cite>, <cite>System Shock</cite>, <cite>Duke Nukem
3D</cite> y <cite>Wolfenstein 3D</cite>.

Ĉi tiaj ludoj ĝojas longan vivon, ĉar ilia kodo estas pli facile
portebla al novaj platformoj kaj kunhavebla kaj aliaj uzantoj.
<cite>DOOM</cite> funkcias en poŝtelefonoj, kameraoj, bankaŭtomatoj kaj
eĉ en panrostiloj! Ĝia heredaĵo estas sekura sen nenia daŭra engaĝiĝo de
la originaj programistoj. Tio ankaŭ permesas derivitajn verkojn &mdash;
novajn ludojn bazitajn sur via kodo &mdash;, kvankam ĝi povus fuĝigi
iujn disvolvistojn. Uzi rajtocedan permesilon kiel la GPL postulas
*ankaŭ* igi la derivitajn verkojn liberaj programoj. La komunumo
ĝenerale ne havas problemon pri tio, sed ĝi povas influi la volon de
estontaj programistoj integri vian laboron en iliaj propraj komercaj
ludoj. Mi persone pensas, ke la disvastigo de liberaj programoj, kiu
estas implicita en la uzo de rajtoceda permesilo estas pozitiva afero
&mdash; sed eble vi volas uzi alian metodon.

## Permesema permesilo, proprietaj havaĵoj

Se vi volas permesi, ke via fontkodo atingu kiom eble plej multaj
estontaj ludoj, permesema liberprograma permesilo kiel [MIT][0] estas la
vojo. [Flotilla](https://github.com/blendogames/flotilla) estas ekzemplo
de ludo, kiu uzis tiun rimedon. Ĝi permesas al programistoj integri vian
fontkodon en iliajn proprajn ludojn sen multaj limigoj, aŭ kreante
rektan derivaĵon aŭ prenante etajn specimenojn de via kodo kaj
integrante ĝin en ilian propran projekton. Tio bezonas nenian devigon
publikigi iliajn proprajn ŝanĝojn aŭ laborojn per simila maniero: ili
povas simple preni ĝin, kun malmulte da kondiĉoj. Tia procedo ege
faciligas integri ĝin en novajn komercajn ludojn.

[0]: https://opensource.org/licenses/MIT

Ĉi tio estas la plej sindonema maniero liberigi vian kodon. Mi
rekomendus ĉi tion, se vi ne zorgas pri tio, kio okazos al via kodo
poste, kaj vi nur volas liberigi ĝin kaj fari alian aferon. Kvankam ĉi
tio certe ebligos al la plej granda nombro da estontaj projektoj uzi
vian laboron, la rajtoceda metodo pli bonas por certigi, ke la eble plej
granda nombro da estontaj ludoj estu *ankaŭ* liberaj.

## Malfermitaj havaĵoj

Se vi sentas vin speciale oferema, vi ankaŭ povas liberigi la havaĵojn.
Bonaj permesiloj por ĉi tio estas la
[Creative Commons](https://creativecommons.org/)-permesiloj. Ĉiuj
el ili permesas la liberan redistribuon de viaj havaĵoj, do estontaj
ludantoj ne devos aĉeti vian ludon por akiri ilin. Ĉi tio povus esti
grava se la distribuada platformo, kiun vi uzis, mortas aŭ se vi ne plu
vivas kaj neniu povas aĉeti el vi &mdash; bone konsideru tion antaŭ
decidi, ke vi preferas konservi vian elcenton de la malkreskantaj vendoj
de viaj havaĵoj dum via ludo maljuniĝas.

Uzi Creative Commons ankaŭ ebligas al vi agordi kiel viaj havaĵoj povas
esti reuzitaj. Vi povas elekti malsamajn CC-permesilojn por regi la
komercigadon de viaj havaĵoj kaj uzadon en derivitaj verkoj. Por permesi
liberan redistribuon kaj nenion pli la CC NC-ND (nekomerce, nemodifite)
sufiĉos. La permesilo CC BY-SA estas la rajtocedo de kreaj komunaĵoj: ĝi
ebligos liberan redistribuon, komercigadon kaj derivitajn verkojn, *se*
la derivitaj verkoj ankaŭ estas kunhavigitaj per la samaj rajtoj. La
permesema metodo estas CC0, kiu estas la analoga maniero publikigi viajn
havaĵojn kiel publikajn havaĵojn.

Permesi derivitajn verkojn kaj rekomercigon de viaj havaĵoj povas ŝpari
al novaj disvolvistoj de ludoj multe da tempo, precipe sendependaj
disvolvistoj kun malgranda buĝeto. Tio ankaŭ bonegas por krei derivitajn
*ludojn*, simile al modifado, kiu ebligas al kreemaj ludantoj miksi
viajn havaĵojn por krei novan ludon aŭ krompakon.

## Kaj kio se mi ne plene posedas mian ludon?

Vi ne povas fordoni la rajtojn pri io, kion vi ne posedas. Se vi
dependas de proprietaj bibliotekoj, de redaktilo de eksteraj liverantoj
aŭ ne posedas la rajtojn de la muziko aŭ bildoj, vi ne povas liberigi
ilin.

En tiu okazo, mi rekomendas publikigi ĉion, kion vi povas, kiel liberan
programon. Tio povus signifi, ke vi fine publikigas rompitan ludon
&mdash; simple ĝi povus ne funkcii, aŭ eĉ ne kompili, sen tiuj havaĵoj.
Ĉi tio bedaŭrindas, sed liberigante ĉion, kion vi povas, vi lasas vian
komunumon en bona pozicio por mem plenigi la mankojn, eble
restrukturigante vian kodon por solvi ilin aŭ anstataŭigante la
proprietajn partojn per liberaj alternativoj. Ĉi tio ankaŭ permesas, ke
la partoj de via ludo, kiuj estas malfermitkodaj, povu esti reuzitaj en
estontaj ludoj.

## Sed trompantoj povus uzi ĝin!

Tio veras. Kaj indas mencii, ke se via ludo havas devigan interretan eron
bazitan sur viaj propraj serviloj, liberigi tiun programon ne multe
sencas, speciale se vi finfine decidas malŝalti tiujn servilojn.

Oni devas kompromisi. Verdire estas tre malfacile malhelpi trompadon en
via ludo. Se vi kreis popularan plurludulan ludon, vi kaj mi ambaŭ
scias, ke estas ankoraŭ trompantoj, kiuj uzos ĝin spite al viaj fortaj
klopodoj. Teni ĝin proprieta ne forpuŝos la trompantojn. Sociaj solvoj
pli bonas &mdash; kiel sistemo por raporti trompantojn aŭ lasi al amikoj
ludi en privataj serviloj.

Liberigi vian kodon povus helpi al mallertaj kodumuloj eltrovi kiel
pli facile trompi en via ludo. Mi ne povas decidi por vi, ĉu la
kompromiso farindas por via ludo, sed mi povas diri al vi, ke la
avantaĝoj malfermi ĝin estas vastaj kaj la efikeco teni ĝin fermita por
malhelpi trompadon estas dubinda.

## Sed mia kodo hontigas!

Ankaŭ tiu de ĉiuj aliuloj. 🙂 Ni ĉiuj scias, ke ludoj havas striktajn
templimojn kaj pura kodo ne estos la 1-a prioritato. Mi garantios al vi,
ke via komunumo estos ĝoje tro okupita por juĝi vin pro la kvalito de
via kodo. La ideo, ke ĝi devas esti unue «purigita» signifas la morton
de multaj projektoj, kiujn oni alie povintus liberigi. Se vi sentas vin
tiel, vi verŝajne neniam satos kaj pro tio neniam malfermos ĝin. Mi
garantias al vi: via ludo pretas por esti liberigita, ne gravas en kiu
stato ĝi estas!

Kromaĵo: Ethan Lee informis min pri kodo vere terura kodo, kiu restis en
<cite>VVVVVV</cite>, kiun vi povas libere konsulti en la
[etikedo 2.2][vvvvvv 2.2]. Ĝi ne estas bonega, sed vi verŝajne ne sciis
tion &mdash; vi nur memoras <cite>VVVVVV</cite> kiel kritike aklamitan
ludon. Ludprogramistoj laboras sub striktaj limoj kaj neniu juĝas ilin
pri tio &mdash; ni nur volas ĝoji.

[vvvvvv 2.2]: https://github.com/TerryCavanagh/vvvvvv/tree/2.2

## Do kion mi devas fari?

Ni vidu la konkretajn paŝojn. Vi unue devas respondi la sekvajn
demandojn:

- Ĉu mi fakte posedas la tutan ludon? Kiujn partojn mi rajtas publikigi
  kiel liberan programaron?
- Ĉu mi igos la kodon koddisponebla, rajtoceda aŭ permeseme licencita?
- Kio pri la havaĵoj? Ĉu proprietaj? Ĉu Creative Commons? Se ĉi tio, kiu
  versio?

Se vi ne certas pri kio plej bonas, mi rekomendus uzi la GPL-permesilon
por la kodo kaj CC BY-SA por la havaĵoj. Tio permesas derivitajn
verkojn, kondiĉe ke ili estas malfermitaj per simila permesilo. Tio
ebligas al la komunumo konstrui sur via laboro, portante ĝin al novaj
platformoj, konstruante prosperanta komunumon de ludmodifantoj kaj
libere kunhavigante viajn havaĵojn, garantiante daŭran heredaĵon al via
ludo. Se vi ŝatus decidi la detalojn per vi mem, relegu la suprajn
komentojn denove kaj elektu la permesilojn, kiujn vi ŝatus uzi por ĉiu
afero antaŭ daŭrigi.

Se vi bezonas helpon kun iu el ĉi tiuj paŝoj aŭ vi havas ian demandon,
[sendu al mi retmesaĝon](mailto:jorgesumle@freakspot.net), kaj mi helpos
vin kiel eble plej bone.

**Publikigi la fontkodon**

Preparu arkivon de via fontkodo kaj aldonu la permesil-dosieron. Se vi
elektis la koddisponeblan metodon, simple skribu
«Aŭtorajto © <vi> <nuna jaro>. Ĉiuj rajtoj rezervitaj.» en teksta
dosiero nomita PERMESILO [angle LICENSE].

Se vi volas rapide fini tion, simple metu la kodon kaj permesilon en
zip- aŭ tar-dosieron kaj lasu ĝin en via retejo. Pli bona metodo, se vi
havas paciencon, estus publikigi ĝin kiel Git-deponejon. Se vi jam uzas
versikontrolon, vi eble volas zorge pripensi, ĉu vi volas publikigi vian
tutan versikontrolan historion &mdash; la respondo povus esti «jes», sed
se vi necertas, la respondo estas probable «ne». Simple faru kopion de
la kodo, forigu la `.git`-dosierujo kaj importu ĝin en novan deponejon,
se vi bezonas fari tion.

Bone kontrolu, ke ne estas registrante neniun artefakton &mdash;
plenumebla dosiero, bibliotekon, ktp. &mdash;, kaj poste alŝutu ĝin al
via preferata gastiga servilo. GitHub estas populara elekto, sed mi
egoisme rekomendus [Sourcehut](https://sourcehut.org) ankaŭ. Se vi
havas tempon, skribu etan LEGUMIN-dosieron [angle README], kiu servu
kiel enkonduko por la projekto.

**Publikigi la havaĵojn**

Se vi elektas lasi la havaĵojn proprietaj, ne estas pli da paŝoj.
Ludantoj povos eltrovi kiel eltiri la havaĵojn el la aĉetita ludo.

Se vi decidas malfermigi ilin, preparu arkivon de viaj havaĵoj. Aldonu
kopion de la permesilo, kiun vi elektis &mdash; ekzemple la Creative
Commons permesilon, kiun vi uzis &mdash; kaj enmetu ĝin en zip-an,
tar-an aŭ similan dosieron. Publikigu ĝin en via retejo kaj, se vi
sentas vin oferema, preparu iujn instrukciojn pri kiel enmeti la
havaĵaron en la ludon, kiam ludanto kompilu vian ludon.

**Rakontu al la mondo!**

Sciigu al ĉiuj, ke vi liberigis vian ludon! Skribu etan artikolon en via
blogo, ligu al la fontkodo kaj al la havaĵoj kaj ĝuu iom pli el la famo,
kiam la gazetaro kaj la komunumo dankas vin por via kontribuo.

Lasta peto rilate al tio: se vi elektas koddisponeblan metodon, menciu
ĝin tiel en viaj publikaj deklaroj. Disponebla kodo *ne* estas la samo
kiel «libera programo» aŭ «malfermitkodo» kaj la distingo gravas.

Kaj nun estas mia vico danki vin: mi tre ĝojas, ke vi publikigis vian
ludon kiel liberan programon! La komunumo estas multe pli riĉa danke al
via kontribuo, kaj mi esperas, ke via ludo vivu dum multaj jaroj,
per portoj kaj modifoj kaj spirite per ĝiaj kontribuoj al estontaj
ludoj. Vi faris ion mirindan. Dankon!
